# 蛋糕销售系统

#### 介绍
本项目是一个蛋糕销售管理系统，主要实现了用户管理、蛋糕管理、类型管理、活动管理、订单管理、销售管理和账号管理等功能。采用 Spring Boot 作为框架，MyBatis 作为 ORM 框架，MySQL 作为数据库，Thymeleaf 作为模板引擎。

#### 技术栈

1.  Spring Boot
2.  MyBatis
3.  MySQL
4.  Thymeleaf
5.  Maven

#### 系统功能

1. 用户管理
用户注册/登录
用户信息修改 
用户密码修改
用户权限管理
2. 类型管理
蛋糕类型添加/修改/删除
蛋糕类型查询
3. 蛋糕管理
蛋糕添加/修改/删除
蛋糕查询
4. 活动管理
活动添加/修改/删除
活动查询
5. 订单管理
订单添加/修改/删除
订单查询
6. 销售管理
销售统计
销售报表生成
7. 账号管理
账号添加/修改/删除
账号权限设置

#### 系统架构

1.  表示层：使用 Thymeleaf 模板引擎渲染页面，提供用户交互界面。
2.  业务逻辑层：使用 Spring 的注解和拦截器实现业务逻辑处理。
3.  数据访问层：使用 MyBatis 实现数据持久化，通过 Mapper 文件和接口实现数据库操作。
4.  数据库：使用 MySQL 存储系统数据。


#### 项目构建

1. 使用 Maven 进行项目构建，在 pom.xml 文件中添加依赖。
2. 创建数据库表，根据需求设计表结构。
3. 创建 Mapper 文件，实现数据库操作接口。
4. 创建 Service 层，实现业务逻辑处理。
5. 创建 Controller 层，实现页面请求的处理和转发。
6. 创建页面模板，使用 Thymeleaf 渲染页面。

#### 注意事项
   本系统为练习系统，源码有偿提供，需要+q:470799628(本系统无商用方案)
#### 系统截图

![img.png](./images/img.png)
![img_1.png](./images/img_1.png)
![img_2.png](./images/img_2.png)
![img_3.png](./images/img_3.png)
![img_4.png](./images/img_4.png)
![img_5.png](./images/img_5.png)
![img_6.png](./images/img_6.png)
![img_7.png](./images/img_7.png)
