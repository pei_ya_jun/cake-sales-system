package priv.project;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("priv.project.common.mapper")
public class CakeShopApplication {

    public static void main(String[] args) {
        // 热部署
        System.setProperty("spring.devtools.restart.enabled", "false");
        SpringApplication.run(CakeShopApplication.class, args);
    }

}
