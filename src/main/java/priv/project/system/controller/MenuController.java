package priv.project.system.controller;

import cn.hutool.core.util.StrUtil;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;
import priv.project.common.entity.*;
import priv.project.common.error.MyErrorModel;
import priv.project.common.service.TMenuAdminRelationService;
import priv.project.common.service.TMenuService;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @Author 斗佛
 * @Date 2022/11/22
 * @description 菜单相关控制器
 * 下一位读我代码的人, 有任何疑问请联系我, QQ：943701114
 */
@RestController
@RequestMapping("/admin/menu")
@AllArgsConstructor
public class MenuController {

    private TMenuAdminRelationService menuAdminRelationService;

    private TMenuService menuService;

    /**
     * 获取当前登录用户的菜单信息
     * @param session
     * @return
     */
    @GetMapping("getMenuInfo")
    public Object getMenuInfo(HttpSession session) {
        try {
            // 获取当前登录用户的数据
            TAdmin admin = (TAdmin) session.getAttribute("userInfo");
            // 根据用户ID查询数据
            List<TMenu> menuList = menuAdminRelationService.getMenuInfo(admin.getId());
            if(menuList == null || menuList.size() == 0) {
                return new ResultMessage<Object>().warn("未查询到信息！");
            }
            menuList.forEach(item -> {
                item.setTitle(item.getMenuName());
                item.setHref(item.getUrl());
            });
            // 返回菜单信息
            return menuList;
        } catch (Exception e) {
            e.printStackTrace();
            return new ResultMessage<Object>().danger("获取数据出现异常！");
        }
    }

    /**
     * 查询所有菜单信息用于生成树状组件信息
     * 同时根据AdminID查询该管理员下的所拥有的菜单信息
     * 将已拥有的菜单信息默认选中
     * @param adminId
     * @return
     */
    @GetMapping("/getAllInfoByTreeBean")
    public ResultMessage<List<LayuiTreeBean>> getAllInfoByTreeBean(Integer adminId) {
        // 查询数据
        try {
            // 查询全部菜单信息
            List<TMenu> allMenuInfo = menuService.getListByParam(null);
            // 根据管理员ID查询已经绑定的管理员菜单信息
            List<TMenuAdminRelation> menuInfoByUserId = menuAdminRelationService.getMenuInfoByAdminId(adminId);
            // 最终返回的树状组件信息
            // 循环处理将所有菜单加入集合
            List<LayuiTreeBean> resultList = handleAllMenuInfo(allMenuInfo);
            // 遍历集合将该用户已有的菜单信息修改为已拥有的状态
            for (LayuiTreeBean layuiTreeBean : resultList) {
                for (TMenuAdminRelation menuAdminRelation : menuInfoByUserId) {
                    if((menuAdminRelation.getMenuId()+"").equals(layuiTreeBean.getId()+"")) {
                        layuiTreeBean.setChecked(true);
                    } else if(layuiTreeBean.getChildren() != null && layuiTreeBean.getChildren().size() > 0) {
                        // 如果是一级菜单, 则需要再将其包含的二级菜单循环一遍
                        for (LayuiTreeBean child : layuiTreeBean.getChildren()) {
                            if((menuAdminRelation.getMenuId()+"").equals(child.getId()+"")) {
                                child.setChecked(true);
                            }
                        }
                    }
                }
            }
            // 封装数据
            return new ResultMessage<List<LayuiTreeBean>>().success("查询成功！", resultList);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResultMessage<List<LayuiTreeBean>>().danger("后台出现异常：" + e.getMessage());
        }
    }

    /**
     * 根据管理员ID分配菜单权限
     * @return
     */
    @PostMapping("/insertInfoByAdminId")
    public ResultMessage<TMenuAdminRelation> insertInfoByAdminId(@RequestBody Map<String, Object> paramMap) {
        try {
            Object adminId = paramMap.get("adminId");
            List<Object> menuIds = (List<Object>) paramMap.get("menuIds");
            menuAdminRelationService.insertInfoByAdminId(Integer.parseInt(adminId + ""), menuIds);
            return new ResultMessage<TMenuAdminRelation>().success("操作成功！");
        } catch (MyErrorModel e) {
            e.printStackTrace();
            return new ResultMessage<TMenuAdminRelation>().warn(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            return new ResultMessage<TMenuAdminRelation>().danger("后台出现错误：" + e.getMessage());
        }
    }

    /**
     * 处理全部菜单信息的集合
     * @param allMenuInfo
     * @return
     */
    private List<LayuiTreeBean> handleAllMenuInfo(List<TMenu> allMenuInfo) {
        List<LayuiTreeBean> resultList = new ArrayList<>(16);
        // 先将所有菜单加入到集合中
        for (TMenu menu : allMenuInfo) {
            // 如果是一集菜单则再寻找他的二级菜单有哪些
            if(StrUtil.isBlankOrUndefined(menu.getUrl())) {
                LayuiTreeBean layuiTreeBean = new LayuiTreeBean();
                layuiTreeBean.setChecked(false);
                layuiTreeBean.setId(menu.getId());
                layuiTreeBean.setTitle(menu.getMenuName());
                layuiTreeBean.setField("menu_name");
                layuiTreeBean.setSpread(true);
                List<LayuiTreeBean> subMenu = new ArrayList<>(16);
                for (TMenu menu1 : allMenuInfo) {
                    // 只寻找二级菜单, 如果是一级直接跳过
                    if(StrUtil.isBlankOrUndefined(menu1.getParentId()+"")) {
                        continue;
                    }
                    // 是否相匹配
                    if(menu1.getParentId().equals(menu.getId())) {
                        LayuiTreeBean subLayuiTreeBean = new LayuiTreeBean();
                        subLayuiTreeBean.setId(menu1.getId());
                        subLayuiTreeBean.setTitle(menu1.getMenuName());
                        subLayuiTreeBean.setField("menu_name");
                        subLayuiTreeBean.setChecked(false);
                        subMenu.add(subLayuiTreeBean);
                    }
                }
                layuiTreeBean.setChildren(subMenu);
                resultList.add(layuiTreeBean);
            } else if("0".equals(menu.getParentId())) {
                LayuiTreeBean layuiTreeBean = new LayuiTreeBean();
                layuiTreeBean.setId(menu.getId());
                layuiTreeBean.setTitle(menu.getMenuName());
                layuiTreeBean.setField("menu_name");
                layuiTreeBean.setChecked(false);
                resultList.add(layuiTreeBean);
            }
        }
        return resultList;
    }
}
