package priv.project.system.controller;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;
import priv.project.common.entity.ResultMessage;
import priv.project.common.entity.TGoodsType;
import priv.project.common.error.MyErrorModel;
import priv.project.common.service.TGoodsTypeService;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @Author 斗佛
 * @Date 2022/12/03
 * @description 类型相关控制器
 * 下一位读我代码的人, 有任何疑问请联系我, QQ：943701114
 */
@RestController
@RequestMapping("/admin/type")
@AllArgsConstructor
public class GoodsTypeController {

    private TGoodsTypeService goodsTypeService;

    /**
     * 分页查询数据
     * @return
     */
    @GetMapping("getListByPage")
    public ResultMessage<List<TGoodsType>> getListByPage(Long page, Long limit, TGoodsType goodsType) {
        try {
            // 调用Service层方法
            IPage<TGoodsType> pageBean = goodsTypeService.getListByPage(page, limit, goodsType);
            // 返回查询结果
            return new ResultMessage<List<TGoodsType>>().successLayuiTable("查询成功!", pageBean.getTotal(), limit, pageBean.getRecords());
        } catch (Exception e) {
            e.printStackTrace();
            return new ResultMessage<List<TGoodsType>>().danger("后台出现错误：" + e.getMessage());
        }
    }

    /**
     * 新增或修改数据
     * @return
     */
    @PostMapping("saveOrUpdate")
    public ResultMessage<TGoodsType> saveOrUpdate(TGoodsType goodsType) {
        try {
            // 判断如果是新增则保存时间和默认未删除的状态
            if(goodsType.getId() == null) {
                // 保存时间
                goodsType.setCreateDate(new Date());
                // 设置删除状态为未删除
                goodsType.setIsDelete("0");
            }
            // 调用Service层分方法
            goodsTypeService.saveOrUpdate(goodsType);
            // 返回查询结果
            return new ResultMessage<TGoodsType>().success("操作成功! ");
        } catch (MyErrorModel e) {
            e.printStackTrace();
            return new ResultMessage<TGoodsType>().warn(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            return new ResultMessage<TGoodsType>().danger("后台出现错误：" + e.getMessage());
        }
    }

    /**
     * 批量删除类型信息
     * @return
     */
    @DeleteMapping("deleteInfo")
    public ResultMessage<TGoodsType> deleteInfo(String ids) {
        try {
            // 将传入的ID截取
            List<String> split = StrUtil.split(ids, ',', true, true);
            // 调用Service层方法
            goodsTypeService.deleteInfo(split);
            // 返回查询结果
            return new ResultMessage<TGoodsType>().success("操作成功! ");
        } catch (MyErrorModel e) {
            e.printStackTrace();
            return new ResultMessage<TGoodsType>().warn(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            return new ResultMessage<TGoodsType>().danger("后台出现错误：" + e.getMessage());
        }
    }

}
