package priv.project.system.controller;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import lombok.AllArgsConstructor;
import org.apache.ibatis.annotations.Param;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import priv.project.common.entity.ResultMessage;
import priv.project.common.entity.TGoods;
import priv.project.common.entity.TUploadFile;
import priv.project.common.error.MyErrorModel;
import priv.project.common.service.TGoodsService;
import priv.project.common.service.TUploadFileService;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @Author 斗佛
 * @Date 2022/12/05
 * @description 商品相关控制器
 * 下一位读我代码的人, 有任何疑问请联系我, QQ：943701114
 */
@RestController
@RequestMapping("/admin/goods")
@AllArgsConstructor
public class GoodsController {

    private TGoodsService goodsService;

    private TUploadFileService uploadFileService;

    /**
     * 分页查询数据
     * @return
     */
    @GetMapping("getListByPage")
    public ResultMessage<List<TGoods>> getListByPage(Long page, Long limit, TGoods goods) {
        try {
            // 调用Service层方法
            IPage<TGoods> pageBean = goodsService.getListByPage(page, limit, goods);
            // 返回查询结果
            return new ResultMessage<List<TGoods>>().successLayuiTable("查询成功!", pageBean.getTotal(), limit, pageBean.getRecords());
        } catch (Exception e) {
            e.printStackTrace();
            return new ResultMessage<List<TGoods>>().danger("后台出现错误：" + e.getMessage());
        }
    }

    /**
     * 根据ID查询
     * @return
     */
    @GetMapping("getInfoById")
    public ResultMessage<TGoods> getInfoById(Integer id) {
        try {
            // 调用Service层方法
            TGoods goods = goodsService.getInfoById(id);
            // 返回查询结果
            return new ResultMessage<TGoods>().success("查询成功!", goods);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResultMessage<TGoods>().danger("后台出现错误：" + e.getMessage());
        }
    }

    /**
     * 新增信息
     * @return
     */
    @PostMapping("saveInfo")
    public ResultMessage<TGoods> saveInfo(@RequestParam("files") MultipartFile[] files, TGoods goods, HttpServletRequest request) {
        try {
            // 调用新增商品方法
            goodsService.insertInfo(files, goods, request);
            return new ResultMessage<TGoods>().success("操作成功! ");
        } catch (Exception e) {
            e.printStackTrace();
            return new ResultMessage<TGoods>().danger("后台出现错误：" + e.getMessage());
        }
    }

    /**
     * 修改信息
     * @return
     */
    @PostMapping("updateInfo")
    public ResultMessage<TGoods> updateInfo(@RequestParam(value = "files", required = false) MultipartFile[] files, TGoods goods, HttpServletRequest request) {
        try {
            // 调用业务层方法
            goodsService.updateInfo(files, goods, request);
            return new ResultMessage<TGoods>().success("操作成功! ");
        } catch (Exception e) {
            e.printStackTrace();
            return new ResultMessage<TGoods>().danger("后台出现错误：" + e.getMessage());
        }
    }

    /**
     * 批量更新上下架状态
     * @return
     */
    @PostMapping("updateGoodsStatus")
    public ResultMessage<TGoods> updateGoodsStatus(String ids, String goodsStatus) {
        try {
            // 将传来的ID进行循环处理
            List<String> split = StrUtil.split(ids, ',', true, true);
            // 用来保存处理完成后得数据
            List<TGoods> goodsList = new ArrayList<>(16);
            for (String item : split) {
                TGoods goods = new TGoods();
                goods.setId(Integer.parseInt(item));
                goods.setGoodsStatus(goodsStatus);
                goodsList.add(goods);
            }
            // 调用业务层方法
            goodsService.updateBatchById(goodsList);
            return new ResultMessage<TGoods>().success("操作成功! ");
        } catch (Exception e) {
            e.printStackTrace();
            return new ResultMessage<TGoods>().danger("后台出现错误：" + e.getMessage());
        }
    }

    /**
     * 批量更新推荐状态
     * @return
     */
    @PostMapping("updateIndexFlag")
    public ResultMessage<TGoods> updateIndexFlag(String id, String indexFlag) {
        try {
            TGoods goods = new TGoods();
            goods.setId(Integer.parseInt(id));
            goods.setIndexFlag(indexFlag);
            // 调用业务层方法
            goodsService.updateById(goods);
            return new ResultMessage<TGoods>().success("操作成功! ");
        } catch (Exception e) {
            e.printStackTrace();
            return new ResultMessage<TGoods>().danger("后台出现错误：" + e.getMessage());
        }
    }


    /**
     * 批量删除类型信息
     * @return
     */
    @DeleteMapping("deleteInfo")
    public ResultMessage<TGoods> deleteInfo(String ids) {
        try {
            // 将传入的ID截取
            List<String> split = StrUtil.split(ids, ',', true, true);
            // 调用Service层方法
            goodsService.deleteInfo(split);
            // 返回查询结果
            return new ResultMessage<TGoods>().success("操作成功! ");
        } catch (MyErrorModel e) {
            e.printStackTrace();
            return new ResultMessage<TGoods>().warn(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            return new ResultMessage<TGoods>().danger("后台出现错误：" + e.getMessage());
        }
    }

    /**
     * 获取上传的图片
     * @return
     */
    @GetMapping("getUploadListByGoodsId")
    public ResultMessage<List<TUploadFile>> getUploadListByGoodsId(Integer goodsId) {
        try {
            // 根据ID查询
            List<TUploadFile> uploadFiles = uploadFileService.getUploadListByGoodsId(goodsId);
            // 返回查询结果
            return new ResultMessage<List<TUploadFile>>().success("操作成功! ", uploadFiles);
        } catch (MyErrorModel e) {
            e.printStackTrace();
            return new ResultMessage<List<TUploadFile>>().warn(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            return new ResultMessage<List<TUploadFile>>().danger("后台出现错误：" + e.getMessage());
        }
    }

}
