package priv.project.system.controller;

import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import priv.project.common.entity.ResultMessage;
import priv.project.common.entity.TDiscount;
import priv.project.common.entity.TUploadFile;
import priv.project.common.error.MyErrorModel;
import priv.project.common.service.TDiscountService;
import priv.project.common.service.TUploadFileService;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

/**
 * @Author 斗佛
 * @Date 2022/12/06
 * @description 商品相关控制器
 * 下一位读我代码的人, 有任何疑问请联系我, QQ：943701114
 */
@RestController
@RequestMapping("/admin/discount")
@AllArgsConstructor
public class DiscountController {

    private TDiscountService discountService;

    /**
     * 分页查询数据
     * @return
     */
    @GetMapping("getListByPage")
    public ResultMessage<List<TDiscount>> getListByPage(Long page, Long limit, TDiscount discount) {
        try {
            // 调用Service层方法
            IPage<TDiscount> pageBean = discountService.getListByPage(page, limit, discount);
            // 返回查询结果
            return new ResultMessage<List<TDiscount>>().successLayuiTable("查询成功!", pageBean.getTotal(), limit, pageBean.getRecords());
        } catch (Exception e) {
            e.printStackTrace();
            return new ResultMessage<List<TDiscount>>().danger("后台出现错误：" + e.getMessage());
        }
    }

    /**
     * 新增数据
     * @return
     */
    @PostMapping("saveInfo")
    public ResultMessage<TDiscount> saveInfo(TDiscount discount) {
        try {
            // 调用Service层分方法
            discountService.saveInfo(discount);
            // 返回查询结果
            return new ResultMessage<TDiscount>().success("操作成功! ");
        } catch (MyErrorModel e) {
            e.printStackTrace();
            return new ResultMessage<TDiscount>().warn(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            return new ResultMessage<TDiscount>().danger("后台出现错误：" + e.getMessage());
        }
    }

    /**
     * 修改数据
     * @return
     */
    @PostMapping("updateInfo")
    public ResultMessage<TDiscount> updateInfo(TDiscount discount) {
        try {
            // 调用Service层分方法
            discountService.updateInfo(discount);
            // 返回查询结果
            return new ResultMessage<TDiscount>().success("操作成功! ");
        } catch (MyErrorModel e) {
            e.printStackTrace();
            return new ResultMessage<TDiscount>().warn(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            return new ResultMessage<TDiscount>().danger("后台出现错误：" + e.getMessage());
        }
    }

    /**
     * 批量删除类型信息
     * @return
     */
    @DeleteMapping("deleteInfo")
    public ResultMessage<TDiscount> deleteInfo(String ids) {
        try {
            // 将传入的ID截取
            List<String> strIds = StrUtil.split(ids, ',', true, true);
            List<Integer> split = new ArrayList<>();
            for (String strId : strIds) {
                split.add(Integer.parseInt(strId));
            }
            // 调用Service层方法
            discountService.deleteInfo(split);
            // 返回查询结果
            return new ResultMessage<TDiscount>().success("操作成功! ");
        } catch (MyErrorModel e) {
            e.printStackTrace();
            return new ResultMessage<TDiscount>().warn(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            return new ResultMessage<TDiscount>().danger("后台出现错误：" + e.getMessage());
        }
    }

}
