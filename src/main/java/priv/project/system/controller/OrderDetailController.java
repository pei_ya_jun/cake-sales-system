package priv.project.system.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import priv.project.common.entity.ResultMessage;
import priv.project.common.entity.TOrderDetail;
import priv.project.common.service.TOrderDetailService;

import java.util.List;

/**
 * @Author 斗佛
 * @Date 2022/12/05
 * @description 订单相关控制器
 * 下一位读我代码的人, 有任何疑问请联系我, QQ：943701114
 */
@RestController
@RequestMapping("/admin/orderDetail")
@AllArgsConstructor
public class OrderDetailController {

    private TOrderDetailService orderDetailService;

    /**
     * 分页查询数据
     * @return
     */
    @GetMapping("getListByPage")
    public ResultMessage<List<TOrderDetail>> getListByPage(Long page, Long limit, TOrderDetail orderDetail) {
        try {
            // 调用Service层方法
            IPage<TOrderDetail> pageBean = orderDetailService.getListByPage(page, limit, orderDetail);
            // 返回查询结果
            return new ResultMessage<List<TOrderDetail>>().successLayuiTable("查询成功!", pageBean.getTotal(), limit, pageBean.getRecords());
        } catch (Exception e) {
            e.printStackTrace();
            return new ResultMessage<List<TOrderDetail>>().danger("后台出现错误：" + e.getMessage());
        }
    }

}
