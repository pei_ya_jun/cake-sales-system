package priv.project.system.controller;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import priv.project.common.entity.ResultMessage;
import priv.project.common.entity.TAdmin;
import priv.project.common.entity.TMenu;
import priv.project.common.entity.TUser;
import priv.project.common.service.TMenuAdminRelationService;
import priv.project.common.service.TMenuService;
import priv.project.common.service.TUserService;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

/**
 * @Author 斗佛
 * @Date 2022/12/03
 * @description 用户相关控制器
 * 下一位读我代码的人, 有任何疑问请联系我, QQ：943701114
 */
@RestController
@RequestMapping("/admin/user")
@AllArgsConstructor
public class UserController {

    private TUserService userService;

    /**
     * 分页查询数据
     * @return
     */
    @GetMapping("getListByPage")
    public ResultMessage<List<TUser>> getListByPage(Long page, Long limit, TUser user) {
        try {
            // 调用Service层方法
            IPage<TUser> pageBean = userService.getListByPage(page, limit, user);
            // 返回查询结果
            return new ResultMessage<List<TUser>>().successLayuiTable("查询成功!", pageBean.getTotal(), limit, pageBean.getRecords());
        } catch (Exception e) {
            e.printStackTrace();
            return new ResultMessage<List<TUser>>().danger("后台出现错误：" + e.getMessage());
        }
    }

    /**
     * 批量更新用户状态
     * @return
     */
    @PostMapping("updateStatus")
    public ResultMessage<TUser> updateStatus(String ids, String userStatus) {
        try {
            // 将传入的ID截取
            List<String> split = StrUtil.split(ids, ',', true, true);
            List<TUser> users = new ArrayList<>(16);
            // 封装条件到实体类中
            for (int i = 0; i < split.size(); i++) {
                TUser user = new TUser();
                user.setId(Integer.parseInt(split.get(i)));
                // 设置状态为解除冻结
                user.setUserStatus(userStatus);
                users.add(user);
            }
            // 调用Service层方法
            userService.updateBatchById(users);
            // 返回查询结果
            return new ResultMessage<TUser>().success("操作成功! ");
        } catch (Exception e) {
            e.printStackTrace();
            return new ResultMessage<TUser>().danger("后台出现错误：" + e.getMessage());
        }
    }

}
