package priv.project.system.controller;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;
import priv.project.common.entity.ResultMessage;
import priv.project.common.entity.TAdmin;
import priv.project.common.entity.TAdmin;
import priv.project.common.error.MyErrorModel;
import priv.project.common.service.TAdminService;
import priv.project.common.util.ServletUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

/**
 * @Author 斗佛
 * @Date 2022/12/03
 * @description 用户相关控制器
 * 下一位读我代码的人, 有任何疑问请联系我, QQ：943701114
 */
@RestController
@RequestMapping("/admin/admin")
@AllArgsConstructor
public class AdminController {

    private final TAdminService adminService;

    /**
     * 分页查询数据
     * @param admin    查询条件
     * @param page     当前第几页
     * @param limit    每天显示多少条
     * @return         返回统一的返回类型
     */
    @GetMapping("/getListByPage")
    public ResultMessage<List<TAdmin>> getListByPage(TAdmin admin, Long page, Long limit, HttpServletRequest request) {
        try {
            IPage<TAdmin> pageInfo = adminService.getListByPage(admin, page, limit, ServletUtils.getAdminIdInfo(request));
            return new ResultMessage<List<TAdmin>>().successLayuiTable("查询成功！", pageInfo.getTotal(), limit, pageInfo.getRecords());
        } catch (Exception e) {
            e.printStackTrace();
            return new ResultMessage<List<TAdmin>>().danger("后台出现异常：" + e.getMessage());
        }
    }

    /**
     * 新增管理员信息
     * @return
     */
    @PostMapping("/insertInfo")
    public ResultMessage<TAdmin> insertInfo(@RequestBody TAdmin admin) {
        try {
            // 调用Service查询方法
            adminService.insertInfo(admin);
            return new ResultMessage<TAdmin>().success("操作成功！");
        } catch (MyErrorModel e) {
            e.printStackTrace();
            return new ResultMessage<TAdmin>().warn(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            return new ResultMessage<TAdmin>().danger("后台出现错误：" + e.getMessage());
        }
    }

    /**
     * 修改管理员信息
     * @return
     */
    @PutMapping("/updateInfo")
    public ResultMessage<TAdmin> updateInfo(@RequestBody TAdmin admin, HttpSession session) {
        try {
            // 调用Service查询方法
            adminService.updateInfo(admin);
            // 删除用户信息重新登录
            session.removeAttribute("userInfo");
            return new ResultMessage<TAdmin>().success("操作成功！");
        } catch (MyErrorModel e) {
            e.printStackTrace();
            return new ResultMessage<TAdmin>().warn(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            return new ResultMessage<TAdmin>().danger("后台出现错误：" + e.getMessage());
        }
    }

    /**
     * 删除信息
     * @return
     */
    @DeleteMapping("/deleteInfo")
    public ResultMessage<TAdmin> deleteInfo(String ids) {
        try {
            // 将传入的ID截取
            List<String> strIds = StrUtil.split(ids, ',', true, true);
            List<Integer> split = new ArrayList<>();
            for (String strId : strIds) {
                split.add(Integer.parseInt(strId));
            }
            // 调用Service删除方法
            adminService.deleteInfo(split);
            return new ResultMessage<TAdmin>().success("操作成功！");
        } catch (MyErrorModel e) {
            e.printStackTrace();
            return new ResultMessage<TAdmin>().warn(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            return new ResultMessage<TAdmin>().danger("后台出现错误：" + e.getMessage());
        }
    }

}
