package priv.project.system.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import lombok.AllArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;
import priv.project.common.entity.ResultMessage;
import priv.project.common.entity.TOrder;
import priv.project.common.service.TOrderService;
import priv.project.common.service.TUploadFileService;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @Author 斗佛
 * @Date 2022/12/05
 * @description 订单相关控制器
 * 下一位读我代码的人, 有任何疑问请联系我, QQ：943701114
 */
@RestController
@RequestMapping("/admin/order")
@AllArgsConstructor
public class OrderController {

    private TOrderService orderService;

    /**
     * 分页查询数据
     * @return
     */
    @GetMapping("getListByPage")
    public ResultMessage<List<TOrder>> getListByPage(Long page, Long limit, TOrder order) {
        try {
            // 调用Service层方法
            IPage<TOrder> pageBean = orderService.getListByPage(page, limit, order);
            // 返回查询结果
            return new ResultMessage<List<TOrder>>().successLayuiTable("查询成功!", pageBean.getTotal(), limit, pageBean.getRecords());
        } catch (Exception e) {
            e.printStackTrace();
            return new ResultMessage<List<TOrder>>().danger("后台出现错误：" + e.getMessage());
        }
    }

    /**
     * 根据参数查询销售数据
     * @return
     */
    @GetMapping("getDataInfoByParam")
    public ResultMessage<Map<String, Object>> getDataInfoByParam(@DateTimeFormat(pattern = "yyyy-MM-dd") Date startTime
                                                                , @DateTimeFormat(pattern = "yyyy-MM-dd") Date endTime) {
        try {
            // 调用service层查询方法
            Map<String, Object> data = orderService.getDataInfoByParam(startTime, endTime);
            return new ResultMessage<Map<String, Object>>().success("查询成功! ", data);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResultMessage<Map<String, Object>>().danger("后台出现错误：" + e.getMessage());
        }
    }

    /**
     * 根据ID查询
     * @return
     */
    @GetMapping("getInfoById")
    public ResultMessage<TOrder> getInfoById(Integer id) {
        try {
            // 调用Service层方法
            TOrder order = orderService.getInfoById(id);
            // 返回查询结果
            return new ResultMessage<TOrder>().success("查询成功!", order);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResultMessage<TOrder>().danger("后台出现错误：" + e.getMessage());
        }
    }

    /**
     * 修改信息
     * @return
     */
    @PostMapping("updateInfo")
    public ResultMessage<TOrder> updateInfo(TOrder order) {
        try {
            // 调用业务层方法
            orderService.updateById(order);
            return new ResultMessage<TOrder>().success("操作成功! ");
        } catch (Exception e) {
            e.printStackTrace();
            return new ResultMessage<TOrder>().danger("后台出现错误：" + e.getMessage());
        }
    }

}
