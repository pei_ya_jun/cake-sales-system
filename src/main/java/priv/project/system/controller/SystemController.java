package priv.project.system.controller;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import priv.project.common.entity.ResultMessage;
import priv.project.common.entity.TAdmin;
import priv.project.common.entity.TDiscount;
import priv.project.common.entity.TGoods;
import priv.project.common.service.*;
import priv.project.common.util.ServletUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;

/**
 * 后台页面控制器， 控制页面之间的跳转，以及登录等操作
 * @author 斗佛
 * @date 2022/11/27
 * @description 下一位读我代码的人, 有任何疑问请联系我, qq: 943701114
 */
@Controller
@AllArgsConstructor
public class SystemController {

    // Admin表的服务对象
    private TAdminService adminService;

    // User表的服务对象
    private TUserService userService;

    // GoodsType表的服务对象
    private TGoodsTypeService goodsTypeService;

    // Goods表的服务对象
    private TGoodsService goodsService;

    // Discount表的服务对象
    private TDiscountService discountService;

    /**
     * 跳转到登录页
     * @return
     */
    @GetMapping("/admin")
    public ModelAndView toLoginPage(ModelAndView modelAndView) {
        modelAndView.setViewName("admin/login");
        return modelAndView;
    }

    /**
     * 跳转到登录页
     * @return
     */
    @PostMapping("/system/logout")
    @ResponseBody
    public ResultMessage<Object> adminLogout(HttpSession session) {
        // 删除当前登录的用户信息
        session.removeAttribute("userInfo");
        return new ResultMessage<Object>().success("已安全退出！");
    }

    /**
     * 跳转到登陆过期页面
     * @return
     */
    @GetMapping("/system/toTimeOutPage")
    public ModelAndView toTimeOutPage(ModelAndView modelAndView) {
        modelAndView.setViewName("admin/timeOut");
        return modelAndView;
    }

    /**
     * 跳转到后台首页
     * @param modelAndView
     * @return
     */
    @GetMapping("/system/index")
    public ModelAndView toIndexPage(ModelAndView modelAndView) {
        modelAndView.setViewName("admin/index");
        return modelAndView;
    }

    /**
     * 跳转到后台欢迎页
     * @param modelAndView
     * @return
     */
    @GetMapping("/system/console")
    public ModelAndView toWelcomePage(ModelAndView modelAndView) {
        modelAndView.setViewName("admin/console/welcome");
        return modelAndView;
    }

    /**
     * 跳转到修改管理员信息页面
     * @param modelAndView
     * @return
     */
    @GetMapping("/admin/admin/toEditPage")
    public ModelAndView toEditAdminPage(ModelAndView modelAndView, HttpServletRequest request) {
        // 根据ID查询排班信息
        TAdmin admin = adminService.getById(ServletUtils.getAdminIdInfo(request));
        modelAndView.addObject("obj", admin);
        // 设置跳转页面
        modelAndView.setViewName("admin/admin/edit");
        return modelAndView;
    }

    // -----------------------------------------------------------------------------------------------------------------------------
    // -------------------------------------------- 控制后台管理页面跳转的控制器 Start -------------------------------------------------
    // -----------------------------------------------------------------------------------------------------------------------------

    /**
     * 跳转到用户管理页面
     * @param modelAndView
     * @return
     */
    @GetMapping("/admin/user/toDataPage")
    public ModelAndView toUserPage(ModelAndView modelAndView) {
        modelAndView.setViewName("admin/user/table");
        return modelAndView;
    }

    /**
     * 跳转到类型管理页面
     * @param modelAndView
     * @return
     */
    @GetMapping("/admin/type/toDataPage")
    public ModelAndView toTypePage(ModelAndView modelAndView) {
        modelAndView.setViewName("admin/type/table");
        return modelAndView;
    }

    /**
     * 跳转到商品管理页面
     * @param modelAndView
     * @return
     */
    @GetMapping("/admin/goods/toDataPage")
    public ModelAndView toGoodsPage(ModelAndView modelAndView) {
        modelAndView.setViewName("admin/goods/table");
        return modelAndView;
    }

    /**
     * 跳转到活动管理页面
     * @param modelAndView
     * @return
     */
    @GetMapping("/admin/discount/toDataPage")
    public ModelAndView toDiscountPage(ModelAndView modelAndView) {
        modelAndView.setViewName("admin/discount/table");
        return modelAndView;
    }

    /**
     * 跳转到订单管理页面
     * @param modelAndView
     * @return
     */
    @GetMapping("/admin/order/toDataPage")
    public ModelAndView toOrderPage(ModelAndView modelAndView) {
        modelAndView.setViewName("admin/order/table");
        return modelAndView;
    }

    /**
     * 跳转到订单详情页面
     * @param modelAndView
     * @return
     */
    @GetMapping("/admin/order/toDetailDataPage")
    public ModelAndView toOrderDetailPage(ModelAndView modelAndView, Integer id) {
        modelAndView.addObject("orderId", id);
        modelAndView.setViewName("admin/order/find");
        return modelAndView;
    }

    /**
     * 跳转到数据统计管理页面
     * @param modelAndView
     * @return
     */
    @GetMapping("/admin/sale/toDataPage")
    public ModelAndView toSalePage(ModelAndView modelAndView) {
        modelAndView.setViewName("admin/sale/table");
        return modelAndView;
    }

    /**
     * 跳转到管理员管理页面
     * @param modelAndView
     * @return
     */
    @GetMapping("/admin/admin/toDataPage")
    public ModelAndView toAdminPage(ModelAndView modelAndView) {
        modelAndView.setViewName("admin/admin/table");
        return modelAndView;
    }

    /**
     * 跳转到新增管理员信息页面
     * @param modelAndView
     * @return
     */
    @GetMapping("/admin/admin/toAddPage")
    public ModelAndView toAddAdminPage(ModelAndView modelAndView) {
        // 设置跳转页面
        modelAndView.setViewName("admin/admin/add");
        return modelAndView;
    }

    /**
     * 跳转到修改管理员权限页面
     * @param modelAndView
     * @return
     */
    @GetMapping("/admin/admin/toPowerPage")
    public ModelAndView toPowerAdminPage(ModelAndView modelAndView, Integer id) {
        modelAndView.addObject("adminId", id);
        // 设置跳转页面
        modelAndView.setViewName("admin/admin/power");
        return modelAndView;
    }

    /**
     * 跳转到新增管理员信息页面
     * @param modelAndView
     * @return
     */
    @GetMapping("/admin/user/toAddPage")
    public ModelAndView toAddUserPage(ModelAndView modelAndView) {
        // 设置跳转页面
        modelAndView.setViewName("admin/user/add");
        return modelAndView;
    }

    /**
     * 跳转到修改管理员信息页面
     * @param modelAndView
     * @return
     */
    @GetMapping("/admin/user/toEditPage")
    public ModelAndView toEditUserPage(ModelAndView modelAndView, Integer id) {
        // 查询并保存要修改的用户信息传入修改页面
        modelAndView.addObject("obj", userService.getById(id));
        // 设置跳转页面
        modelAndView.setViewName("admin/user/edit");
        return modelAndView;
    }

    /**
     * 跳转到新增类型信息页面
     * @param modelAndView
     * @return
     */
    @GetMapping("/admin/type/toAddPage")
    public ModelAndView toAddTypePage(ModelAndView modelAndView) {
        // 设置跳转页面
        modelAndView.setViewName("admin/type/add");
        return modelAndView;
    }

    /**
     * 跳转到修改类型信息页面
     * @param modelAndView
     * @return
     */
    @GetMapping("/admin/type/toEditPage")
    public ModelAndView toEditTypePage(ModelAndView modelAndView, Integer id) {
        modelAndView.addObject("data", goodsTypeService.getById(id));
        // 设置跳转页面
        modelAndView.setViewName("admin/type/edit");
        return modelAndView;
    }

    /**
     * 跳转到新增商品信息页面
     * @param modelAndView
     * @return
     */
    @GetMapping("/admin/goods/toAddPage")
    public ModelAndView toAddGoodsPage(ModelAndView modelAndView) {
        // 查询全部类型信息, 用于商品新增时进行类型的绑定
        modelAndView.addObject("goodsType", goodsTypeService.getListInfo());
        // 设置跳转页面
        modelAndView.setViewName("admin/goods/add");
        return modelAndView;
    }

    /**
     * 跳转到修改商品信息页面
     * @param modelAndView
     * @return
     */
    @GetMapping("/admin/goods/toEditPage")
    public ModelAndView toEditGoodsPage(ModelAndView modelAndView, Integer id) {
        TGoods goods = goodsService.getInfoById(id);
        // 查询要修改的信息
        modelAndView.addObject("data", goods);
        // 查询全部类型信息, 用于商品新增时进行类型的绑定
        modelAndView.addObject("goodsType", goodsTypeService.getListInfo());
        // 设置跳转页面
        modelAndView.setViewName("admin/goods/edit");
        return modelAndView;
    }

    /**
     * 跳转到新增活动信息页面
     * @param modelAndView
     * @return
     */
    @GetMapping("/admin/discount/toAddPage")
    public ModelAndView toAddDiscountPage(ModelAndView modelAndView) {
        // 查询全部已上架商品信息, 用于选择折扣商品
        modelAndView.addObject("goodsList", goodsService.getListByParam(new TGoods()));
        // 设置跳转页面
        modelAndView.setViewName("admin/discount/add");
        return modelAndView;
    }

    /**
     * 跳转到修改活动信息页面
     * @param modelAndView
     * @return
     */
    @GetMapping("/admin/discount/toEditPage")
    public ModelAndView toEditDiscountPage(ModelAndView modelAndView, Integer id) {
        TDiscount discount = discountService.getInfoById(id);
        // 查询要修改的信息
        modelAndView.addObject("data", discount);
        // 查询全部已上架商品信息, 用于选择折扣商品
        modelAndView.addObject("goodsList", goodsService.getListByParam(new TGoods()));
        // 设置跳转页面
        modelAndView.setViewName("admin/discount/edit");
        return modelAndView;
    }

    // -----------------------------------------------------------------------------------------------------------------------------
    // -------------------------------------------- 控制后台管理页面跳转的控制器 End -------------------------------------------------
    // -----------------------------------------------------------------------------------------------------------------------------

}
