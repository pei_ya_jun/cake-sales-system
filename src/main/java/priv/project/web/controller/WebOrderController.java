package priv.project.web.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import priv.project.common.entity.ResultMessage;
import priv.project.common.entity.TOrder;
import priv.project.common.error.MyErrorModel;
import priv.project.common.service.TAddressService;
import priv.project.common.service.TOrderService;
import priv.project.common.util.ServletUtils;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @Author 斗佛
 * @Date 2023/02/09
 * @description 订单相关控制器
 * 下一位读我代码的人, 有任何疑问请联系我, QQ：943701114
 */
@RestController
@RequestMapping("/web/order")
@AllArgsConstructor
public class WebOrderController {

    private final TOrderService orderService;
    
    /**
     * 分页查询数据
     * @return
     */
    @GetMapping("/getListByPage")
    public ResultMessage<List<TOrder>> getListByPage(Long page, Long limit, TOrder order, HttpServletRequest request) {
        try {
            // 为地址绑定当前用户ID
            order.setUserId(ServletUtils.getWebUserInfoId(request.getSession()));
            // 调用Service层方法
            IPage<TOrder> pageBean = orderService.getListByPage(page, limit, order);
            // 返回查询结果
            return new ResultMessage<List<TOrder>>().successTable("查询成功!", pageBean.getTotal(), limit, pageBean.getPages(), pageBean.getRecords());
        } catch (Exception e) {
            e.printStackTrace();
            return new ResultMessage<List<TOrder>>().danger("后台出现错误：" + e.getMessage());
        }
    }
    
    /**
     * 下单接口
     * @param addressId     下单收货地址
     * @param checkoutType  下单类型 0单个商品下单  1购物车批量下单
     * @return
     */
    @PostMapping("/saveInfo")
    public ResultMessage<TOrder> saveInfo(Integer addressId, Integer checkoutType, Integer goodsId, HttpServletRequest request) {
        try {
            // 判断下单类型
            if(checkoutType == 0) {
                // 调用业务层方法处理
                orderService.saveInfoByOne(addressId, goodsId, ServletUtils.getWebUserInfo(request.getSession()));
            } else if(checkoutType == 1) {
                // 调用业务层方法处理
                orderService.saveInfoByCart(addressId, ServletUtils.getWebUserInfo(request.getSession()));
            }
            return new ResultMessage<TOrder>().success("操作成功！");
        } catch (MyErrorModel e) {
            e.printStackTrace();
            return new ResultMessage<TOrder>().warn(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            return new ResultMessage<TOrder>().danger("后台异常：" + e.getMessage());
        }
    }

    /**
     * 修改订单状态
     * @param order
     * @return
     */
    @PostMapping("/updateInfo")
    public ResultMessage<TOrder> updateInfo(TOrder order, HttpServletRequest request) {
        try {
            // 调用业务层方法处理
            orderService.updateById(order);
            return new ResultMessage<TOrder>().success("操作成功！");
        } catch (MyErrorModel e) {
            e.printStackTrace();
            return new ResultMessage<TOrder>().warn(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            return new ResultMessage<TOrder>().danger("后台异常：" + e.getMessage());
        }
    }

    /**
     * 用户删除订单
     * @param order
     * @return
     */
    @PostMapping("/deleteInfo")
    public ResultMessage<TOrder> deleteInfo(TOrder order) {
        try {
            // 将订单信息修改为已删除
            order.setUserDelFlag("1");
            // 调用业务层方法处理
            orderService.updateById(order);
            return new ResultMessage<TOrder>().success("操作成功！");
        } catch (MyErrorModel e) {
            e.printStackTrace();
            return new ResultMessage<TOrder>().warn(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            return new ResultMessage<TOrder>().danger("后台异常：" + e.getMessage());
        }
    }
}
