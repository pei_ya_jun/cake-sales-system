package priv.project.web.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;
import priv.project.common.entity.TCart;
import priv.project.common.entity.TGoods;
import priv.project.common.entity.TGoodsType;
import priv.project.common.service.TAddressService;
import priv.project.common.service.TCartService;
import priv.project.common.service.TGoodsService;
import priv.project.common.service.TGoodsTypeService;
import priv.project.common.util.ServletUtils;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

/**
 * 用户端页面控制器， 控制页面之间的跳转
 * @author 斗佛
 * @date 2023/02/03
 * @description 下一位读我代码的人, 有任何疑问请联系我, qq: 943701114
 */
@Controller
@AllArgsConstructor
public class WebController {

    // 商品表业务层
    private final TGoodsService goodsService;

    // 商品类型表业务层
    private final TGoodsTypeService goodsTypeService;

    // 购物车表业务层
    private final TCartService cartService;

    // 收货地址表业务层
    private final TAddressService addressService;

    /**
     * 跳转到用户端首页
     * @return
     */
    @GetMapping(value = {"/", "/index"})
    public ModelAndView toWebIndexPage(ModelAndView modelAndView) {
        // 查询首页推荐的信息
        List<TGoods> indexGoodsList = goodsService.getIndexGoodsList();
        // 查询十个最新发布的商品
        List<TGoods> newsGoodsList = goodsService.getNewsGoodsList();
        modelAndView.addObject("indexGoodsList", indexGoodsList);
        modelAndView.addObject("newsGoodsList", newsGoodsList);
        modelAndView.addObject("showRecommend", "1");
        modelAndView.setViewName("web/index");
        return modelAndView;
    }

    /**
     * 跳转到商城页
     * @return
     */
    @GetMapping("/web/store")
    public ModelAndView toSearchPage(ModelAndView modelAndView) {
        // 查询商品类型
        List<TGoodsType> listInfo = goodsTypeService.getListInfo();
        modelAndView.addObject("goodsTypeList", listInfo);
        modelAndView.addObject("showRecommend", "0");
        modelAndView.setViewName("web/store");
        return modelAndView;
    }

    /**
     * 跳转到登录页
     * @param modelAndView
     * @return
     */
    @GetMapping("/web/login")
    public ModelAndView toLoginPage(ModelAndView modelAndView) {
        modelAndView.setViewName("web/login");
        return modelAndView;
    }

    /**
     * 跳转到注册页
     * @param modelAndView
     * @return
     */
    @GetMapping("/web/resister")
    public ModelAndView toResisterPage(ModelAndView modelAndView) {
        modelAndView.setViewName("web/resister");
        return modelAndView;
    }

    /**
     * 跳转到新闻详情页
     * @return
     */
    @GetMapping("/web/detail")
    public ModelAndView toDetailPage(ModelAndView modelAndView, Integer id, HttpServletRequest request) {
        modelAndView.addObject("showRecommend", "0");
        modelAndView.addObject("item", goodsService.getInfoById(id));
        modelAndView.setViewName("web/details");
        return modelAndView;
    }

    /**
     * 跳转到购物车
     * @return
     */
    @GetMapping("/web/cart")
    public ModelAndView toCartPage(ModelAndView modelAndView, HttpServletRequest request) {
        // 根据登录用户信息查询购物车信息
        modelAndView.addObject("cartInfo", cartService.getCartInfoByUserId(ServletUtils.getWebUserInfoId(request.getSession())));
        modelAndView.setViewName("web/cart");
        return modelAndView;
    }

    /**
     * 跳转到结算页面
     * @return
     */
    @GetMapping("/web/checkout")
    public ModelAndView toCloseAccountPage(ModelAndView modelAndView, HttpServletRequest request, Integer checkoutType, Integer goodsId) {
        // 用来计算最终价格的变量
        Integer resultPrice = 0;
        // 用来计算原价的变量
        Integer costPrice = 0;
        // 判断结算类型, 0单个商品结算 1购物车整体结算
        if(checkoutType == 0) {
            // 单个商品结算, 首先查询该商品信息
            TGoods goodsInfo = goodsService.getInfoById(goodsId);
            // 创建临时的购物车对象
            List<TCart> cartList = new ArrayList<>();
            // 封装商品信息
            TCart cart = new TCart();
            cart.setGoodsId(goodsId);
            cart.setUserId(ServletUtils.getWebUserInfoId(request.getSession()));
            cart.setGoodsCount(1);
            cart.setGoodsName(goodsInfo.getGoodsName());
            cart.setGoodsPrice(goodsInfo.getGoodsDiscountPrice() == null ? goodsInfo.getGoodsPrice() : goodsInfo.getGoodsDiscountPrice());
            cart.setGoodsTotal(cart.getGoodsPrice() * cart.getGoodsCount());
            cart.setGoods(goodsInfo);
            cartList.add(cart);
            // 设置最终价
            resultPrice = cart.getGoodsTotal();
            costPrice = goodsInfo.getGoodsPrice() * cart.getGoodsCount();
            modelAndView.addObject("cartInfo", cartList);
        } else {
            // 查询购物车中该用户的所有商品
            List<TCart> cartList = cartService.getCartInfoByUserId(ServletUtils.getWebUserInfoId(request.getSession()));
            // 计算出所有购物车商品中的小计价格
            for (TCart item : cartList) {
                // 判断商品是否有活动价, 如果有活动价则按照活动价计算
                if(item.getGoods().getGoodsDiscountPrice() == null) {
                    // 没有活动价
                    item.setGoodsTotal(item.getGoods().getGoodsPrice() * item.getGoodsCount());
                } else {
                    // 有活动价
                    item.setGoodsTotal(item.getGoods().getGoodsDiscountPrice() * item.getGoodsCount());
                }
                // 累加所有商品的原价
                costPrice += item.getGoods().getGoodsPrice() * item.getGoodsCount();
                // 累加最终结算价格
                resultPrice += item.getGoodsTotal();
            }
            modelAndView.addObject("cartInfo", cartList);
        }
        // 查询用户的默认地址
        modelAndView.addObject("addressInfo", addressService.getUserDefaultAddress(ServletUtils.getWebUserInfoId(request.getSession())));
        // 返回数据
        modelAndView.addObject("checkoutType", checkoutType);
        modelAndView.addObject("goodsId", goodsId);
        modelAndView.addObject("costPrice", costPrice);
        // 计算出总计优惠了多少钱
        modelAndView.addObject("discountPrice", costPrice - resultPrice);
        modelAndView.addObject("resultPrice", resultPrice);
        modelAndView.setViewName("web/closeAccount");
        return modelAndView;
    }

    /**
     * 跳转到个人中心页面
     * @return
     */
    @GetMapping("/web/personCenter")
    public ModelAndView toPersonCenter(ModelAndView modelAndView) {
        modelAndView.setViewName("web/personalCenter");
        return modelAndView;
    }

}
