package priv.project.web.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import priv.project.common.entity.ResultMessage;
import priv.project.common.entity.TAddress;
import priv.project.common.entity.TGoods;
import priv.project.common.entity.TAddress;
import priv.project.common.error.MyErrorModel;
import priv.project.common.service.TAddressService;
import priv.project.common.util.ServletUtils;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @Author 斗佛
 * @Date 2023/02/09
 * @description 用户收货地址相关控制器
 * 下一位读我代码的人, 有任何疑问请联系我, QQ：943701114
 */
@RestController
@RequestMapping("/web/address")
@AllArgsConstructor
public class WebAddressController {

    private final TAddressService addressService;
    
    /**
     * 分页查询数据
     * @return
     */
    @GetMapping("/getListByPage")
    public ResultMessage<List<TAddress>> getListByPage(Long page, Long limit, TAddress address, HttpServletRequest request) {
        try {
            // 为地址绑定当前用户ID
            address.setUserId(ServletUtils.getWebUserInfoId(request.getSession()));
            // 调用Service层方法
            IPage<TAddress> pageBean = addressService.getListByPage(page, limit, address);
            // 返回查询结果
            return new ResultMessage<List<TAddress>>().successTable("查询成功!", pageBean.getTotal(), limit, pageBean.getPages(), pageBean.getRecords());
        } catch (Exception e) {
            e.printStackTrace();
            return new ResultMessage<List<TAddress>>().danger("后台出现错误：" + e.getMessage());
        }
    }

    /**
     * 根据ID查询单条信息
     * @param id
     * @return
     */
    @GetMapping("/getInfoById")
    public ResultMessage<TAddress> getInfoById(Integer id) {
        try {
            // 调用查询方法
            TAddress address = addressService.getById(id);
            return new ResultMessage<TAddress>().success("查询成功！", address);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResultMessage<TAddress>().danger("后台出现错误：" + e.getMessage());
        }
    }
    
    /**
     * 保存地址
     * @param address
     * @return
     */
    @PostMapping("/saveOrUpdate")
    public ResultMessage<TAddress> saveOrUpdateInfo(TAddress address, HttpServletRequest request) {
        try {
            // 为地址绑定当前用户ID
            address.setUserId(ServletUtils.getWebUserInfoId(request.getSession()));
            // 调用业务层方法处理
            addressService.saveOrUpdateInfo(address);
            return new ResultMessage<TAddress>().success("操作成功！");
        } catch (MyErrorModel e) {
            e.printStackTrace();
            return new ResultMessage<TAddress>().warn(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            return new ResultMessage<TAddress>().danger("后台异常：" + e.getMessage());
        }
    }

    /**
     * 删除地址
     * @param id
     * @return
     */
    @PostMapping("/deleteInfo")
    public ResultMessage<TAddress> deleteInfo(Integer id) {
        try {
            // 调用业务层方法处理
            addressService.removeById(id);
            return new ResultMessage<TAddress>().success("操作成功！");
        } catch (MyErrorModel e) {
            e.printStackTrace();
            return new ResultMessage<TAddress>().warn(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            return new ResultMessage<TAddress>().danger("后台异常：" + e.getMessage());
        }
    }



}
