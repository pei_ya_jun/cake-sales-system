package priv.project.web.controller;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import priv.project.common.entity.ResultMessage;
import priv.project.common.entity.TGoods;
import priv.project.common.entity.TUploadFile;
import priv.project.common.error.MyErrorModel;
import priv.project.common.service.TGoodsService;
import priv.project.common.service.TUploadFileService;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

/**
 * @Author 斗佛
 * @Date 2023/02/03
 * @description 商品相关控制器
 * 下一位读我代码的人, 有任何疑问请联系我, QQ：943701114
 */
@RestController
@RequestMapping("/web/goods")
@AllArgsConstructor
public class WebGoodsController {

    private TGoodsService goodsService;

    private TUploadFileService uploadFileService;

    /**
     * 分页查询数据
     * @return
     */
    @GetMapping("getWebInfoByPage")
    public ResultMessage<List<TGoods>> getWebInfoByPage(Long page, Long limit, TGoods goods) {
        try {
            // 调用Service层方法
            IPage<TGoods> pageBean = goodsService.getListByPage(page, limit, goods);
            // 返回查询结果
            return new ResultMessage<List<TGoods>>().successTable("查询成功!", pageBean.getTotal(), limit, pageBean.getPages(), pageBean.getRecords());
        } catch (Exception e) {
            e.printStackTrace();
            return new ResultMessage<List<TGoods>>().danger("后台出现错误：" + e.getMessage());
        }
    }

    /**
     * 根据ID查询
     * @return
     */
    @GetMapping("getInfoById")
    public ResultMessage<TGoods> getInfoById(Integer id) {
        try {
            // 调用Service层方法
            TGoods goods = goodsService.getInfoById(id);
            // 返回查询结果
            return new ResultMessage<TGoods>().success("查询成功!", goods);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResultMessage<TGoods>().danger("后台出现错误：" + e.getMessage());
        }
    }

    /**
     * 获取上传的图片
     * @return
     */
    @GetMapping("getUploadListByGoodsId")
    public ResultMessage<List<TUploadFile>> getUploadListByGoodsId(Integer goodsId) {
        try {
            // 根据ID查询
            List<TUploadFile> uploadFiles = uploadFileService.getUploadListByGoodsId(goodsId);
            // 返回查询结果
            return new ResultMessage<List<TUploadFile>>().success("操作成功! ", uploadFiles);
        } catch (MyErrorModel e) {
            e.printStackTrace();
            return new ResultMessage<List<TUploadFile>>().warn(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            return new ResultMessage<List<TUploadFile>>().danger("后台出现错误：" + e.getMessage());
        }
    }

}
