package priv.project.web.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import priv.project.common.entity.ResultMessage;
import priv.project.common.entity.TCart;
import priv.project.common.entity.TGoods;
import priv.project.common.entity.TUploadFile;
import priv.project.common.error.MyErrorModel;
import priv.project.common.service.TCartService;
import priv.project.common.service.TGoodsService;
import priv.project.common.service.TUploadFileService;
import priv.project.common.util.ServletUtils;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @Author 斗佛
 * @Date 2023/02/03
 * @description 购物车相关控制器
 * 下一位读我代码的人, 有任何疑问请联系我, QQ：943701114
 */
@RestController
@RequestMapping("/web/cart")
@AllArgsConstructor
public class WebCartController {

    private final TCartService cartService;

    /**
     * 分页查询数据
     * @return
     */
    @GetMapping("getCartInfoByUserId")
    public ResultMessage<List<TCart>> getCartInfoByUserId(HttpServletRequest request) {
        try {
            // 调用Service层方法
            List<TCart> data = cartService.getCartInfoByUserId(ServletUtils.getWebUserInfoId(request.getSession()));
            // 返回查询结果
            return new ResultMessage<List<TCart>>().success("查询成功!", data);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResultMessage<List<TCart>>().danger("后台出现错误：" + e.getMessage());
        }
    }

    /**
     * 加入购物车
     * @return
     */
    @PostMapping("saveCartInfo")
    public ResultMessage<TCart> saveCartInfo(Integer goodsId, HttpServletRequest request) {
        try {
            // 根据ID查询
            cartService.saveCartInfo(goodsId, ServletUtils.getWebUserInfoId(request.getSession()));
            // 返回查询结果
            return new ResultMessage<TCart>().success("操作成功! ");
        } catch (MyErrorModel e) {
            e.printStackTrace();
            return new ResultMessage<TCart>().warn(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            return new ResultMessage<TCart>().danger("后台出现错误：" + e.getMessage());
        }
    }

    /**
     * 删除购物车单条信息
     * @return
     */
    @PostMapping("deleteInfoById")
    public ResultMessage<TCart> deleteInfoById(Integer id) {
        try {
            // 调用删除方法
            cartService.removeById(id);
            // 返回查询结果
            return new ResultMessage<TCart>().success("操作成功! ");
        } catch (MyErrorModel e) {
            e.printStackTrace();
            return new ResultMessage<TCart>().warn(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            return new ResultMessage<TCart>().danger("后台出现错误：" + e.getMessage());
        }
    }

    /**
     * 清空购物车
     * @return
     */
    @PostMapping("deleteInfoByAll")
    public ResultMessage<TCart> deleteInfoByAll(HttpServletRequest request) {
        try {
            // 获取当前登录用户的ID
            Integer userId = ServletUtils.getWebUserInfoId(request.getSession());
            // 调用删除方法
            QueryWrapper<TCart> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("user_id", userId);
            cartService.remove(queryWrapper);
            // 返回查询结果
            return new ResultMessage<TCart>().success("操作成功! ");
        } catch (MyErrorModel e) {
            e.printStackTrace();
            return new ResultMessage<TCart>().warn(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            return new ResultMessage<TCart>().danger("后台出现错误：" + e.getMessage());
        }
    }

}
