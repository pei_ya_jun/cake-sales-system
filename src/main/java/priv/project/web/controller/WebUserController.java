package priv.project.web.controller;

import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import priv.project.common.entity.ResultMessage;
import priv.project.common.entity.TUser;
import priv.project.common.error.MyErrorModel;
import priv.project.common.service.TUserService;
import priv.project.common.util.ServletUtils;

import javax.servlet.http.HttpServletRequest;

/**
 * @Author 斗佛
 * @Date 2023/02/07
 * @description 用户相关控制器
 * 下一位读我代码的人, 有任何疑问请联系我, QQ：943701114
 */
@RestController
@RequestMapping("/web/user")
@AllArgsConstructor
public class WebUserController {

    private final TUserService userService;

    /**
     * 保存用户信息 - 注册
     * @param user
     * @return
     */
    @PostMapping("/saveInfo")
    public ResultMessage<TUser> saveInfo(TUser user, HttpServletRequest request) {
        try {
            // 调用业务层方法处理
            TUser saveUser = userService.saveInfo(user);
            // 注册成功, 自动登录
            if(saveUser != null) {
                request.getSession().setAttribute("webUserInfo", user);
            }
            return new ResultMessage<TUser>().success("注册成功！已为您自动登录！");
        } catch (MyErrorModel e) {
            e.printStackTrace();
            return new ResultMessage<TUser>().warn(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            return new ResultMessage<TUser>().danger("后台异常：" + e.getMessage());
        }
    }

    /**
     * 修改用户信息
     * @param user
     * @return
     */
    @PostMapping("/updateInfo")
    public ResultMessage<TUser> updateInfo(TUser user, HttpServletRequest request) {
        try {
            // 设置用户ID
            user.setId(ServletUtils.getWebUserInfoId(request.getSession()));
            // 调用修改方法
            userService.updateById(user);
            // 重新查询用户信息
            TUser userInfo = userService.getById(user.getId());
            // 重新保存用户信息
            request.getSession().setAttribute("webUserInfo", userInfo);
            return new ResultMessage<TUser>().success("注册成功！已为您自动登录！");
        } catch (MyErrorModel e) {
            e.printStackTrace();
            return new ResultMessage<TUser>().warn(e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            return new ResultMessage<TUser>().danger("后台异常：" + e.getMessage());
        }
    }

}
