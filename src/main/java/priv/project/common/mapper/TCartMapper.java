package priv.project.common.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import priv.project.common.entity.TCart;
import priv.project.common.entity.TUploadFile;

import java.util.List;

/**
 * 购物车表Mapper层
 * @author 斗佛
 * @date 2022/11/27
 * @description 下一位读我代码的人, 有任何疑问请联系我, qq: 943701114
 */
public interface TCartMapper extends BaseMapper<TCart> {

    /**
     * 根据用户ID查询购物车信息
     * @param webUserInfoId
     * @return
     */
    List<TCart> getCartInfoByUserId(Integer webUserInfoId);

    /**
     * 根据ID查询单条
     * @return
     */
    TUploadFile getUploadFileByGoodsId(Integer id);
}