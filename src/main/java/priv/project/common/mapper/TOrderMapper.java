package priv.project.common.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import priv.project.common.entity.TOrder;
import priv.project.common.entity.TOrderDetail;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 订单表Mapper层
 * @author 斗佛
 * @date 2022/11/27
 * @description 下一位读我代码的人, 有任何疑问请联系我, qq: 943701114
 */
public interface TOrderMapper extends BaseMapper<TOrder> {

    /**
     * 分页查询信息
     * @param pageParam
     * @param order
     * @return
     */
    IPage<TOrder> getListByPage(Page<TOrder> pageParam, @Param("item") TOrder order);

    /**
     * 根据订单ID查询订单详情
     * @return
     */
    List<TOrderDetail> getOrderDetailByOrderId(Integer orderId);

    /**
     * 根据订单ID查询单条详情信息
     * @param id
     * @return
     */
    TOrder getInfoById(Integer id);

    /**
     * 根据参数查询销售数据
     * @param startTime
     * @param endTime
     * @return
     */
    List<Map<String, Object>> getDataInfoByParam(@Param("startTime") Date startTime, @Param("endTime") Date endTime);
}