package priv.project.common.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import org.apache.ibatis.annotations.Param;
import priv.project.common.entity.TDiscount;

import java.util.List;

/**
 * 折扣表Mapper层
 * @author 斗佛
 * @date 2022/11/27
 * @description 下一位读我代码的人, 有任何疑问请联系我, qq: 943701114
 */
public interface TDiscountMapper extends BaseMapper<TDiscount> {

    /**
     * 分页查询信息
     * @param pageParam
     * @param discount
     * @return
     */
    IPage<TDiscount> getListByPage(IPage<TDiscount> pageParam, @Param("item") TDiscount discount);

    /**
     * 根据传入的ID查询活动已经开始的信息
     * @return
     */
    List<TDiscount> getListByParam(@Param("idList") List<Integer> idList);

    /**
     * 根据传入的ID查询活动已经开始的信息
     * @return
     */
    TDiscount getInfoById(Integer id);

    /**
     * 根据传入的时间和商品ID查询该商品在这个相关时间段中是不是已经有了活动
     * @param discount
     * @return
     */
    List<TDiscount> getInfoByDateAndGoodsId(TDiscount discount);
}