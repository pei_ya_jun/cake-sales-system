package priv.project.common.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import org.apache.ibatis.annotations.Param;
import priv.project.common.entity.TGoods;
import priv.project.common.entity.TUploadFile;

import java.util.List;

/**
 * 商品表Mapper层
 * @author 斗佛
 * @date 2022/11/27
 * @description 下一位读我代码的人, 有任何疑问请联系我, qq: 943701114
 */
public interface TGoodsMapper extends BaseMapper<TGoods> {

    /**
     * 分页查询
     * @param pageParam
     * @param goods
     * @return
     */
    IPage<TGoods> getListByPage(IPage<TGoods> pageParam, @Param("item") TGoods goods);

    /**
     * 根据ID查询单条
     * @return
     */
    TGoods getInfoById(Integer id);

    /**
     * 根据ID查询单条
     * @return
     */
    TUploadFile getUploadFileByGoodsId(Integer id);

    /**
     * 根据参数查询所有已上架商品
     * @param goods
     * @return
     */
    List<TGoods> getListByParam(TGoods goods);

    /**
     * 查询首页推荐的商品
     * @return
     */
    List<TGoods> getIndexGoodsList();

    /**
     * 查询最新发布的十个的商品
     * @return
     */
    List<TGoods> getNewsGoodsList();
}