package priv.project.common.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import priv.project.common.entity.TUploadFile;

/**
 * @author 斗佛
 * @date 2022/12/5
 **/
public interface TUploadFileMapper extends BaseMapper<TUploadFile> {
}