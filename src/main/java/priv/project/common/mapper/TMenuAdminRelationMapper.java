package priv.project.common.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import priv.project.common.entity.TMenu;
import priv.project.common.entity.TMenuAdminRelation;

import java.util.List;

/**
 * 管理员与菜单关系表Mapper层
 * @author 斗佛
 * @date 2022/11/27
 * @description 下一位读我代码的人, 有任何疑问请联系我, qq: 943701114
 */
public interface TMenuAdminRelationMapper extends BaseMapper<TMenuAdminRelation> {

    /**
     * 根据用户ID查询菜单信息
     * @param id
     * @return
     */
    List<TMenu> getMenuInfoByUserId(Integer id);

    /**
     * 根据用户ID查询菜单信息
     * @param id
     * @return
     */
    List<TMenu> getMenuInfoByParentId(Integer id);
}