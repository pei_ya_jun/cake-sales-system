package priv.project.common.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import priv.project.common.entity.TUser;

/**
 * 用户表Mapper层
 * @author 斗佛
 * @date 2022/11/27
 * @description 下一位读我代码的人, 有任何疑问请联系我, qq: 943701114
 */
public interface TUserMapper extends BaseMapper<TUser> {
}