package priv.project.common.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * @author 斗佛
 * @date 2022/12/5
 **/
@Data
@TableName(value = "t_upload_file")
public class TUploadFile {
    /**
     * 主键ID
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 对应的商品ID
     */
    @TableField(value = "goods_id")
    private Integer goodsId;

    /**
     * 图片访问地址
     */
    @TableField(value = "file_url")
    private String fileUrl;

    /**
     * 文件名
     */
    @TableField(value = "file_name")
    private String fileName;

    /**
     * 文件大小
     */
    @TableField(value = "file_size")
    private Long fileSize;
}