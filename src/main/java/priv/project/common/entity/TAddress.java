package priv.project.common.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * @author 斗佛
 * @date 2022/11/27
 * @description 下一位读我代码的人, 有任何疑问请联系我, qq: 943701114
* 收货地址表
*/
@Data
@TableName(value = "t_address")
public class TAddress {
    /**
     * 自增主键
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 对应的用户ID
     */
    @TableField(value = "user_id")
    private Integer userId;

    /**
     * 收货人名字
     */
    @TableField(value = "address_name")
    private String addressName;

    /**
     * 收件人手机号
     */
    @TableField(value = "phone")
    private String phone;

    /**
     * 省份编号
     */
    @TableField(value = "province_num")
    private String provinceNum;

    /**
     * 城市编号
     */
    @TableField(value = "city_num")
    private String cityNum;

    /**
     * 区县编号
     */
    @TableField(value = "county_num")
    private String countyNum;

    /**
     * 拼接后的完整地址
     */
    @TableField(value = "address_short")
    private String addressShort;

    /**
     * 是否是默认地址 0不是  1是
     */
    @TableField(value = "is_default")
    private String isDefault;

    /**
     * 详细地址
     */
    @TableField(value = "location_address")
    private String locationAddress;
}