package priv.project.common.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * @author 斗佛
 * @date 2022/11/27
 * @description 下一位读我代码的人, 有任何疑问请联系我, qq: 943701114
* 菜单与管理员管理表
*/
@Data
@TableName(value = "t_menu_admin_relation")
public class TMenuAdminRelation {
    /**
     * 自增主键
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 管理员ID
     */
    @TableField(value = "admin_id")
    private Integer adminId;

    /**
     * 菜单ID
     */
    @TableField(value = "menu_id")
    private Integer menuId;
}