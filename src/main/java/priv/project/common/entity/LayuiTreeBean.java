package priv.project.common.entity;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @Author 斗佛
 * @Date 2022/4/13
 * @description 下一位读我代码的人, 有任何疑问请联系我, qq：943701114
 * Layui穿梭框组件的属性参数封装
 */
@Data
public class LayuiTreeBean implements Serializable {

    /**
     * 属性选项 	    说明 	                                                类型 	        示例值
     * title 	    节点标题 	                                            String 	        未命名
     * id 	        节点唯一索引值，用于对指定节点进行各类操作 	                String/Number 	任意唯一的字符或数字
     * field 	    节点字段名 	                                            String 	        一般对应表字段名
     * children     子节点。支持设定选项同父节点 	                            Array 	        [{title: '子节点1', id: '111'}]
     * href 	    点击节点弹出新窗口对应的 url。需开启 isJump 参数 	        String 	        任意 URL
     * spread 	    节点是否初始展开，默认 false 	                            Boolean 	    true
     * checked 	    节点是否初始为选中状态（如果开启复选框的话），默认 false 	    Boolean 	    true
     * disabled 	节点是否为禁用状态。默认 false 	                        Boolean 	    false
     */

    private String id;

    private String title;

    private String field;

    private List<LayuiTreeBean> children;

    private String href;

    private Boolean spread;

    private Boolean checked;

    private Boolean disabled;
}
