package priv.project.common.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * @author 斗佛
 * @date 2022/11/27
 * @description 下一位读我代码的人, 有任何疑问请联系我, qq: 943701114
* 订单详情表
*/
@Data
@TableName(value = "t_order_detail")
public class TOrderDetail {
    /**
     * 自增主键
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 对应的订单号
     */
    @TableField(value = "order_id")
    private Integer orderId;

    /**
     * 对应的下单商品ID
     */
    @TableField(value = "goods_id")
    private Integer goodsId;

    /**
     * 下单数量
     */
    @TableField(value = "goods_num")
    private Integer goodsNum;

    /**
     * 小计
     */
    @TableField(value = "total_price")
    private Integer totalPrice;

    /**
     * 商品名
     */
    @TableField(value = "goods_name")
    private String goodsName;

    /**
     * 商品单价
     */
    @TableField(value = "goods_price")
    private Integer goodsPrice;
}