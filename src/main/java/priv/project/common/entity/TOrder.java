package priv.project.common.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * @author 斗佛
 * @date 2022/11/27
 * @description 下一位读我代码的人, 有任何疑问请联系我, qq: 943701114
* 订单表
*/
@Data
@TableName(value = "t_order")
public class TOrder {
    /**
     * 自增主键
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 手动生层的订单号
     */
    @TableField(value = "order_num")
    private String orderNum;

    /**
     * 订单价格（单位分）
     */
    @TableField(value = "order_price")
    private Integer orderPrice;

    /**
     * 对应的用户ID
     */
    @TableField(value = "user_id")
    private Integer userId;

    /**
     * 对应的收货地址ID
     */
    @TableField(value = "address_id")
    private Integer addressId;

    /**
     * 用户昵称
     */
    @TableField(value = "user_nick_name")
    private String userNickName;

    /**
     * 收货人
     */
    @TableField(value = "address_name")
    private String addressName;

    /**
     * 下单时间
     */
    @TableField(value = "create_date")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createDate;

    /**
     * 保存地址的JSON串备份
     */
    @TableField(value = "address_json_str")
    private String addressJsonStr;

    /**
     * 配送状态（0代发货  1送货中  2已送达）
     */
    @TableField(value = "delivery_status")
    private String deliveryStatus;

    /**
     * 标记用户是否删除 0未删除  1已删除
     */
    @TableField(value = "user_del_flag")
    private String userDelFlag;

    /**
     * 订单相关的详情信息
     */
    @TableField(exist = false)
    private List<TOrderDetail> orderDetailList;

    /**
     * 收货地址相关信息
     */
    @TableField(exist = false)
    private TAddress addressInfo;
}