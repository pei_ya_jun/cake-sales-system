package priv.project.common.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * @author 斗佛
 * @date 2022/11/27
 * @description 下一位读我代码的人, 有任何疑问请联系我, qq: 943701114
* 购物车表
*/
@Data
@TableName(value = "t_cart")
public class TCart {
    /**
     * 自增主键
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 对应的用户ID
     */
    @TableField(value = "user_id")
    private Integer userId;

    /**
     * 对应的商品ID
     */
    @TableField(value = "goods_id")
    private Integer goodsId;

    /**
     * 对应的商品信息
     */
    @TableField(exist = false)
    private TGoods goods;

    /**
     * 商品名
     */
    @TableField(value = "goods_name")
    private String goodsName;

    /**
     * 商品单价
     */
    @TableField(value = "goods_price")
    private Integer goodsPrice;

    /**
     * 购买数量
     */
    @TableField(value = "goods_count")
    private Integer goodsCount;

    /**
     * 价格小计
     */
    @TableField(value = "goods_total")
    private Integer goodsTotal;

    /**
     * 商品首图展示图名字
     */
    @TableField(value = "goods_head_img")
    private String goodsHeadImg;
}