package priv.project.common.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * @author 斗佛
 * @date 2022/11/27
 * @description 下一位读我代码的人, 有任何疑问请联系我, qq: 943701114
* 折扣表
*/
@Data
@TableName(value = "t_discount")
public class TDiscount {
    /**
     * 自增主键
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 折扣开始时间
     */
    @TableField(value = "start_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date startTime;

    /**
     * 折扣结束时间
     */
    @TableField(value = "end_time")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date endTime;

    /**
     * 优惠金额（存人民币最小单位：分）
     */
    @TableField(value = "price")
    private Integer price;

    /**
     * 对应的商品ID
     */
    @TableField(value = "goods_id")
    private Integer goodsId;

    /**
     * 折扣商品的名字
     */
    @TableField(exist = false)
    private String goodsName;

    /**
     * 折扣商品的原售价
     */
    @TableField(exist = false)
    private Integer goodsPrice;

    /**
     * 折扣商品的成本价
     */
    @TableField(exist = false)
    private Integer goodsCostPrice;

    /**
     * 活动状态  0未开始  1进行中  2已结束
     */
    @TableField(exist = false)
    private String status;
}