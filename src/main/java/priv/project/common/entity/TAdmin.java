package priv.project.common.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * @author 斗佛
 * @date 2022/11/27
 * @description 下一位读我代码的人, 有任何疑问请联系我, qq: 943701114
* 后台管理员表
*/
@Data
@TableName(value = "t_admin")
public class TAdmin {
    /**
     * 自增主键
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 登录名
     */
    @TableField(value = "user_name")
    private String userName;

    /**
     * 密码（MD5加密）
     */
    @TableField(value = "password")
    private String password;

    /**
     * 昵称
     */
    @TableField(value = "nick_name")
    private String nickName;

    /**
     * 登录时输入的验证码
     */
    @TableField(exist = false)
    private String code;

    /**
     * 登录成功后生成的token令牌, 用来校验后续操作时是否已经登录过期
     */
    @TableField(exist = false)
    private String token;

    /**
     * 旧密码
     * 修改管理员密码时使用
     */
    @TableField(exist = false)
    private String oldPwd;
}