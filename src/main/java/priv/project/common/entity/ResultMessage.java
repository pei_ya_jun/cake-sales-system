package priv.project.common.entity;


import lombok.Data;
import priv.project.common.constants.StatusCodeConstants;

/**
 * @author 斗佛
 * @date 2022/11/27
 * @description 下一位读我代码的人, 有任何疑问请联系我, qq: 943701114
 * 统一的返回对象
 **/
@Data
public class ResultMessage<T> {

    private Integer code;

    private String msg;

    private T data;

    // 总页数
    private Long pages;

    // 每页显示多少条数据 - 分页查询时返回
    private Long limit;

    // 分页的总数据量 - 分页查询时返回
    private Long count;

    public ResultMessage() {

    }

    public ResultMessage(Integer code, String msg) {
        this.msg = msg;
        this.code = code;
    }

    public ResultMessage(Integer code, String msg, T data) {
        this.msg = msg;
        this.code = code;
        this.data = data;
    }

    public ResultMessage(Integer code, String msg, Long count, Long limit, T data) {
        this.code = code;
        this.msg = msg;
        this.data = data;
        this.limit = limit;
        this.count = count;
    }

    public ResultMessage(Integer code, String msg, Long count, Long limit, Long pages, T data) {
        this.code = code;
        this.msg = msg;
        this.data = data;
        this.limit = limit;
        this.count = count;
        this.pages = pages;
    }

    /**
     * 正常成功提示同时需要返回分页表格数据
     * @return
     */
    public ResultMessage<T> successTable(String msg, Long count, Long limit, T data) {
        return new ResultMessage<T>(StatusCodeConstants.STATUS_CODE.SUCCESS.getValue(), msg, count, limit, data);
    }

    /**
     * 正常成功提示同时需要返回分页表格数据
     * @return
     */
    public ResultMessage<T> successTable(String msg, Long count, Long limit, Long pages, T data) {
        return new ResultMessage<T>(StatusCodeConstants.STATUS_CODE.SUCCESS.getValue(), msg, count, limit, pages, data);
    }

    /**
     * 正常成功提示同时需要返回分页表格数据
     * @return
     */
    public ResultMessage<T> successLayuiTable(String msg, Long count, Long limit, T data) {
        return new ResultMessage<T>(StatusCodeConstants.STATUS_CODE.SUCCESS_LAYUI.getValue(), msg, count, limit, data);
    }

    /**
     * 正常成功提示同时需要返回数据
     * @return
     */
    public ResultMessage<T> success(String msg, T data) {
        return new ResultMessage<T>(StatusCodeConstants.STATUS_CODE.SUCCESS.getValue(), msg, data);
    }

    /**
     * 正常成功提示
     * @return
     */
    public ResultMessage<T> success(String msg) {
        return new ResultMessage<T>(StatusCodeConstants.STATUS_CODE.SUCCESS.getValue(), msg);
    }

    /**
     * 警告提示
     * @return
     */
    public ResultMessage<T> warn(String msg) {
        return new ResultMessage<T>(StatusCodeConstants.STATUS_CODE.WARN.getValue(), msg);
    }

    /**
     * 错误提示
     * @param msg
     * @return
     */
    public ResultMessage<T> danger(String msg) {
        return new ResultMessage<T>(StatusCodeConstants.STATUS_CODE.ERROR.getValue(), msg);
    }

}
