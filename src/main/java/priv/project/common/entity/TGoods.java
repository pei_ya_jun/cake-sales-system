package priv.project.common.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import org.apache.tomcat.util.http.fileupload.FileUpload;

import java.util.List;

/**
 * @author 斗佛
 * @date 2022/11/27
 * @description 下一位读我代码的人, 有任何疑问请联系我, qq: 943701114
* 商品表
*/
@Data
@TableName(value = "t_goods")
public class TGoods {
    /**
     * 自增主键
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 商品名
     */
    @TableField(value = "goods_name")
    private String goodsName;

    /**
     * 对应类型ID
     */
    @TableField(value = "goods_type_id")
    private Integer goodsTypeId;

    /**
     * 商品类型
     */
    @TableField(exist = false)
    private String goodsType;

    /**
     * 商品售价
     */
    @TableField(value = "goods_price")
    private Integer goodsPrice;

    /**
     * 商品活动价
     */
    private Integer goodsDiscountPrice;

    /**
     * 成本价
     */
    @TableField(value = "goods_cost_price")
    private Integer goodsCostPrice;

    /**
     * 是否上架（0否  1是）
     */
    @TableField(value = "goods_status")
    private String goodsStatus;

    /**
     * 商品介绍
     */
    @TableField(value = "goods_readme")
    private String goodsReadme;

    /**
     * 商品规格介绍
     */
    @TableField(value = "goods_norms")
    private String goodsNorms;

    /**
     * 逻辑删除 (0否  1是)
     */
    @TableField(value = "is_delete")
    private String isDelete;

    /**
     * 是否首页推荐 (0否  1是)
     */
    @TableField(value = "index_flag")
    private String indexFlag;

    /**
     * 相关的文件信息
     */
    @TableField(exist = false)
    private List<TUploadFile> uploadFiles;

    /**
     * 等待删除的文件
     */
    @TableField(exist = false)
    private List<TUploadFile> waitDeleteList;
}