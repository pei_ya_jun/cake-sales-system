package priv.project.common.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import java.util.List;

/**
 * @author 斗佛
 * @date 2022/11/27
 * @description 下一位读我代码的人, 有任何疑问请联系我, qq: 943701114
* 菜单表
*/
@Data
@TableName(value = "t_menu")
public class TMenu {
    /**
     * 主键ID
     */
    @TableId(value = "id", type = IdType.INPUT)
    private String id;

    /**
     * 菜单名
     */
    @TableField(value = "menu_name")
    private String menuName;

    /**
     * 菜单类型 0一级菜单 1二级菜单
     */
    @TableField(value = "type")
    private String type;

    /**
     * 菜单跳转路径
     */
    @TableField(value = "url")
    private String url;

    /**
     * 打开方式  _iframe表示嵌套框打开
     */
    @TableField(value = "open_type")
    private String openType;

    /**
     * 父级菜单的ID
     */
    @TableField(value = "parent_id")
    private String parentId;

    /**
     * 菜单图标
     */
    @TableField(value = "icon")
    private String icon;

    /**
     * 菜单排序位置
     */
    @TableField(value = "sort")
    private Integer sort;

    /**
     * 创建时间
     */
    @TableField(value = "create_date")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createDate;

    @TableField(exist = false)
    private String title;

    @TableField(exist = false)
    private String href;

    @TableField(exist = false)
    private List<TMenu> children;
}