package priv.project.common.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * @author 斗佛
 * @date 2022/11/27
 * @description 下一位读我代码的人, 有任何疑问请联系我, qq: 943701114
* 用户表
*/
@Data
@TableName(value = "t_user")
public class TUser {
    /**
     * 自增主键
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 用户登录名
     */
    @TableField(value = "user_name")
    private String userName;

    /**
     * MD5+密码sha256+盐值sha1+盐值混合加密
     */
    @TableField(value = "password")
    private String password;

    /**
     * 昵称
     */
    @TableField(value = "nick_name")
    private String nickName;

    /**
     * 用户状态：1已冻结  0正常
     */
    @TableField(value = "user_status")
    private String userStatus;

    /**
     * 用户注册时间
     */
    @TableField(value = "create_date")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createDate;

    /**
     * 用户手机号
     */
    @TableField(value = "phone")
    private String phone;

    /**
     * 登录时输入的验证码
     */
    @TableField(exist = false)
    private String code;

    /**
     * 旧密码
     * 修改管理员密码时使用
     */
    @TableField(exist = false)
    private String oldPwd;
}