package priv.project.common.util;


import priv.project.common.entity.TAdmin;
import priv.project.common.entity.TUser;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Enumeration;

/**
 * @author 斗佛Uncle
 * @Description 下一位读我代码的人, 有任何疑问请联系我, qq：943701114
 */
public class ServletUtils {

    /**
     * 获取项目网络地址
     * http://ip:port/projectName/
     * @param request
     * @return
     */
    public static String getProjectHttpUrl(HttpServletRequest request, boolean isIpv4) {
        // 获取项目名称
        String contextPath = request.getContextPath();
        // 获取协议
        String scheme = request.getScheme();
        // 获取ip
        String serverName = request.getServerName();
        if("localhost".equalsIgnoreCase(serverName)) {
            serverName = "127.0.0.1";
        }
        // 获取端口
        int serverPort = request.getServerPort();
        String showPath = scheme + "://" + serverName + ":" + serverPort + contextPath + "/";
        if(isIpv4) {
            return scheme + "://" + getIpv4Address() + ":" + serverPort + contextPath + "/";
        } else {
            return scheme + "://" + serverName + ":" + serverPort + contextPath + "/";
        }
    }

    /**
     * 获取IPv4地址
     * @return
     */
    public static String getIpv4Address() {
        //得到本机所有的网络接口
        Enumeration<NetworkInterface> allNetInterfaces = null;
        String ipv4 = "";
        try {
            allNetInterfaces = NetworkInterface.getNetworkInterfaces();
            //循环遍历所有网络接口
            while (allNetInterfaces.hasMoreElements()) {
                //顺序拿取某个网络接口
                NetworkInterface netInterface = allNetInterfaces.nextElement();
                //获得与该网络接口绑定的 IP 地址，一般只有一个
                Enumeration<InetAddress> addresses = netInterface.getInetAddresses();
                while (addresses.hasMoreElements()) {
                    InetAddress ip = addresses.nextElement();
                    if (ip != null && ip instanceof Inet4Address
                            && !ip.isLoopbackAddress() //loopback地址即本机地址，IPv4的loopback范围是127.0.0.0 ~ 127.255.255.255
                            && ip.getHostAddress().indexOf(":") == -1) {
                        System.out.println();
                        ipv4 = ip.getHostAddress();
                    }
                }
            }
        } catch (SocketException e) {
            e.printStackTrace();
        }
        return ipv4;
    }

    /**
     * 获取后台登陆用户的信息
     * @param request
     * @return
     */
    public static TAdmin getAdminInfo(HttpServletRequest request) {
        HttpSession session = request.getSession();
        TAdmin admin = (TAdmin) session.getAttribute("userInfo");
        return admin;
    }

    /**
     * 获取当前后台已登录用户的ID信息
     * @param request
     * @return
     */
    public static Integer getAdminIdInfo(HttpServletRequest request) {
        HttpSession session = request.getSession();
        TAdmin admin = (TAdmin) session.getAttribute("userInfo");
        if(admin == null) {
            return null;
        }
        return admin.getId();
    }

    /**
     * 获取前台登录的用户的ID信息
     * @param session
     * @return
     */
    public static Integer getWebUserInfoId(HttpSession session) {
        TUser user = (TUser) session.getAttribute("webUserInfo");
        if(user == null) {
            return null;
        }
        return user.getId();
    }

    /**
     * 获取前台登录的用户的信息
     * @param session
     * @return
     */
    public static TUser getWebUserInfo(HttpSession session) {
        TUser user = (TUser) session.getAttribute("webUserInfo");
        if(user == null) {
            return null;
        }
        return user;
    }
}
