package priv.project.common.util;

import cn.hutool.core.lang.UUID;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;

/**
 * @author 斗佛
 * @date 2022/12/6
 * @Description 下一位读我代码的人, 有任何疑问请联系我, qq：943701114
 * 公共工具类
 **/
public class PublicUtil {

    /**
     * 公共的上传图片方法, 最终会返回上传后的文件名
     */
    public static String uploadImageFile(String path, MultipartFile file) throws IOException {
        // 获取上传文件的名字
        String fileName = UUID.randomUUID().toString().replace("-", "") + ".jpg";
        // 创建文件
        File dest = new File(path + fileName);
        // 检测目录是否存在
        if (!dest.getParentFile().exists()) {
            // 不存在就创建
            dest.getParentFile().mkdir();
        }
        // 文件写入
        file.transferTo(dest);
        return fileName;
    }

}
