package priv.project.common.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.*;

/**
 * @author 斗佛
 * @date 2022/11/27
 * @description 下一位读我代码的人, 有任何疑问请联系我, qq: 943701114
 * WebMvc相关配置
 */
@Configuration
public class WebMvcConfig implements WebMvcConfigurer {

    /**
     * 设置首页的跳转
     * 将该方法返回的WebMvcConfigurer添加进Spring容器
     * @return
     */
    @Bean
    public WebMvcConfigurer webMvcConfigurer() {
        WebMvcConfigurer webMvcConfigurer = new WebMvcConfigurer() {

            @Override
            public void addResourceHandlers(ResourceHandlerRegistry registry) {
                // 配置不拦截静态资源
                registry.addResourceHandler("/**").addResourceLocations("classpath:/static/");
            }

            @Override
            public void addCorsMappings(CorsRegistry registry) {
                registry.addMapping("/**")
                        .allowCredentials(true)
                        .allowedHeaders("*")
                        .allowedOriginPatterns("*")
                        .allowedMethods("*")
                        .maxAge(3600);
            }

            /**
             * 扩展添加视图映射
             * @param registry
             */
            @Override
            public void addViewControllers(ViewControllerRegistry registry) {
                // registry.addViewController("/").setViewName("admin/login");   // 拦截根目录请求
            }

            /**
             * 添加拦截器
             * @param registry
             */
            @Override
            public void addInterceptors(InterceptorRegistry registry) {
                registry.addInterceptor(getLoginInterceptor())
                        .addPathPatterns("/**")     // 下面配置忽略拦截哪些
                        .excludePathPatterns("/", "/admin", "/static/**", "/component/**", "/adminStatic/**", "/webStatic/**","/config/**"
                                            , "/static/upload/**", "/util/**", "/index", "/web/store", "/web/detail", "/web/personCenter", "/web/**"
                                            , "/web/goods/**", "/web/login", "/web/resister", "/web/cart/getCartInfoByUserId", "/web/cart/saveCartInfo"
                                            , "/css/**", "/js/**", "/lib/**", "/images/**", "/fonts/**", "/upload/**", "/error/**", "/favicon.ico"
                                            , "/system/toLogin", "/system/login", "/system/logout", "/system/toTimeOutPage", "/system/console", "/rest/login/**");
            }
        };
        return webMvcConfigurer;
    }

    @Bean
    public LoginInterceptor getLoginInterceptor() {
        return new LoginInterceptor();
    }

}