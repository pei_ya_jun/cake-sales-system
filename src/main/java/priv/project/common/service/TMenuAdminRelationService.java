package priv.project.common.service;

import priv.project.common.entity.TMenu;
import priv.project.common.entity.TMenuAdminRelation;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * 管理员与菜单表Service接口层
 * @author 斗佛
 * @date 2022/11/27
 * @description 下一位读我代码的人, 有任何疑问请联系我, qq: 943701114
 */
public interface TMenuAdminRelationService extends IService<TMenuAdminRelation>{

    /**
     * 根据用户ID查询菜单信息
     * @param id
     * @return
     */
    List<TMenu> getMenuInfo(Integer id);

    /**
     * 根据管理员ID分配菜单权限
     * @param adminId
     * @param menuIds
     */
    void insertInfoByAdminId(Integer adminId, List<Object> menuIds);

    /**
     * 根据管理员ID查询已经绑定的管理员菜单信息
     * @param adminId
     * @return
     */
    List<TMenuAdminRelation> getMenuInfoByAdminId(Integer adminId);
}
