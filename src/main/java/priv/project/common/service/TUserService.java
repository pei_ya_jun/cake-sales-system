package priv.project.common.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import priv.project.common.entity.TUser;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * 用户表Service接口层
 * @author 斗佛
 * @date 2022/11/27
 * @description 下一位读我代码的人, 有任何疑问请联系我, qq: 943701114
 */
public interface TUserService extends IService<TUser>{

    /**
     * 分页查询所有用户信息
     * @param page
     * @param limit
     * @param user
     * @return
     */
    IPage<TUser> getListByPage(Long page, Long limit, TUser user);

    /**
     * 根据用户名查询该用户的信息
     * @param userName
     * @return
     */
    List<TUser> selectInfoByUserName(String userName);

    /**
     * 保存用户信息
     * @param user
     * @return
     */
    TUser saveInfo(TUser user);
}
