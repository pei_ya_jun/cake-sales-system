package priv.project.common.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import priv.project.common.entity.TAddress;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 收货地址表Service接口层
 * @author 斗佛
 * @date 2022/11/27
 * @description 下一位读我代码的人, 有任何疑问请联系我, qq: 943701114
 */
public interface TAddressService extends IService<TAddress>{

    /**
     * 保存地址
     * @param address
     * @return
     */
    void saveOrUpdateInfo(TAddress address);

    /**
     * 分页查询信息
     * @param page
     * @param limit
     * @param address
     * @return
     */
    IPage<TAddress> getListByPage(Long page, Long limit, TAddress address);

    /**
     * 根据用户ID查询该用户的默认地址
     * @param webUserInfoId
     * @return
     */
    TAddress getUserDefaultAddress(Integer webUserInfoId);
}
