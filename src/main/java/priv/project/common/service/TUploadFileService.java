package priv.project.common.service;

import priv.project.common.entity.TUploadFile;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * @author 斗佛
 * @date 2022/12/5
 **/
public interface TUploadFileService extends IService<TUploadFile> {

    /**
     * 根据商品ID查询商品下的图片
     * @param goodsId
     * @return
     */
    List<TUploadFile> getUploadListByGoodsId(Integer goodsId);
}
