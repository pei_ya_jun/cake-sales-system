package priv.project.common.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import priv.project.common.entity.TOrderDetail;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 订单详情表Service接口层
 * @author 斗佛
 * @date 2022/11/27
 * @description 下一位读我代码的人, 有任何疑问请联系我, qq: 943701114
 */
public interface TOrderDetailService extends IService<TOrderDetail>{

    /**
     * 分页查询信息
     * @param page
     * @param limit
     * @param orderDetail
     * @return
     */
    IPage<TOrderDetail> getListByPage(Long page, Long limit, TOrderDetail orderDetail);

}
