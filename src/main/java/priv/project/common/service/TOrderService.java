package priv.project.common.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import priv.project.common.entity.TOrder;
import com.baomidou.mybatisplus.extension.service.IService;
import priv.project.common.entity.TUser;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 订单表Service接口层
 * @author 斗佛
 * @date 2022/11/27
 * @description 下一位读我代码的人, 有任何疑问请联系我, qq: 943701114
 */
public interface TOrderService extends IService<TOrder>{


    /**
     * 分页查询数据
     * @param page
     * @param limit
     * @param order
     * @return
     */
    IPage<TOrder> getListByPage(Long page, Long limit, TOrder order);

    /**
     * 根据ID查询单条详情
     * @param id
     * @return
     */
    TOrder getInfoById(Integer id);

    /**
     * 根据参数查询销售数据
     * @param startTime
     * @param endTime
     * @return
     */
    Map<String, Object> getDataInfoByParam(Date startTime, Date endTime);

    /**
     * 单个商品下单
     * @param addressId
     * @param webUserInfo
     */
    void saveInfoByOne(Integer addressId, Integer goodsId, TUser webUserInfo);

    /**
     * 购物车下单
     * @param addressId
     * @param webUserInfo
     */
    void saveInfoByCart(Integer addressId, TUser webUserInfo);
}
