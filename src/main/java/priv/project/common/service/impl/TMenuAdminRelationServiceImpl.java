package priv.project.common.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import priv.project.common.entity.TMenu;
import priv.project.common.entity.TMenuAdminRelation;
import priv.project.common.mapper.TMenuAdminRelationMapper;
import priv.project.common.service.TMenuAdminRelationService;

/**
 * 管理员与菜单关系表Service
 * @author 斗佛
 * @date 2022/11/27
 * @description 下一位读我代码的人, 有任何疑问请联系我, qq: 943701114
 */
@Service
@AllArgsConstructor
public class TMenuAdminRelationServiceImpl extends ServiceImpl<TMenuAdminRelationMapper, TMenuAdminRelation> implements TMenuAdminRelationService{

    private TMenuAdminRelationMapper menuAdminRelationMapper;

    /**
     * 根据用户ID查询菜单信息
     * @param id
     * @return
     */
    @Override
    public List<TMenu> getMenuInfo(Integer id) {
        // 调用查询方法
        return menuAdminRelationMapper.getMenuInfoByUserId(id);
    }

    /**
     * 根据管理员ID分配菜单权限
     * @param adminId
     * @param menuIds
     */
    @Override
    public void insertInfoByAdminId(Integer adminId, List<Object> menuIds) {
        // 首先将该管理员的现有数据全部删除, 之后在重新插入即可
        this.remove(new QueryWrapper<TMenuAdminRelation>().eq("admin_id", adminId));

        // 处理要插入的数据
        List<TMenuAdminRelation> insertInfos = new ArrayList<>(16);
        for (Object item : menuIds) {
            TMenuAdminRelation insertInfo = new TMenuAdminRelation();
            insertInfo.setMenuId(Integer.parseInt(item.toString()));
            insertInfo.setAdminId(adminId);
            insertInfos.add(insertInfo);
        }
        // 调用批量插入方法
        this.saveBatch(insertInfos);
    }

    /**
     * 根据管理员ID查询已经绑定的管理员菜单信息
     * @param adminId
     * @return
     */
    @Override
    public List<TMenuAdminRelation> getMenuInfoByAdminId(Integer adminId) {
        QueryWrapper<TMenuAdminRelation> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("admin_id", adminId);
        return menuAdminRelationMapper.selectList(queryWrapper);
    }
}
