package priv.project.common.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import priv.project.common.mapper.TAddressMapper;
import priv.project.common.entity.TAddress;
import priv.project.common.service.TAddressService;

/**
 * 收货地址表Service
 * @author 斗佛
 * @date 2022/11/27
 * @description 下一位读我代码的人, 有任何疑问请联系我, qq: 943701114
 */
@Service
@AllArgsConstructor
public class TAddressServiceImpl extends ServiceImpl<TAddressMapper, TAddress> implements TAddressService{

    private final TAddressMapper addressMapper;

    /**
     * 保存地址
     * @param address
     * @return
     */
    @Override
    public void saveOrUpdateInfo(TAddress address) {
        // 判断是新增还是修改
        if(address.getId() == null) {
            // 新增, 判断该用户是否有收货地址, 如果没有则将本条设置为默认收货地址
            // 根据用户ID查询数量
            QueryWrapper<TAddress> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("user_id", address.getUserId());
            Long count = addressMapper.selectCount(queryWrapper);
            if(count == 0) {
                // 设置为默认地址
                address.setIsDefault("1");
            } else {
                // 不是默认地址
                address.setIsDefault("0");
            }
        } else {
            // 如果是修改, 则判断是否是将这条信息修改为默认地址
            if("1".equals(address.getIsDefault())) {
                // 是修改默认地址, 则将之前的所有记录先全部更新为不是默认地址
                TAddress updAddress = new TAddress();
                updAddress.setIsDefault("0");
                // 创建条件对象
                QueryWrapper<TAddress> queryWrapper = new QueryWrapper<>();
                queryWrapper.eq("user_id", address.getUserId());
                this.update(updAddress, queryWrapper);
            }
        }
        this.saveOrUpdate(address);
    }

    /**
     * 分页查询信息
     * @param page
     * @param limit
     * @param address
     * @return
     */
    @Override
    public IPage<TAddress> getListByPage(Long page, Long limit, TAddress address) {
        // 创建分页对象
        IPage<TAddress> pageParam = new Page<>(page, limit);
        // 创建条件对象
        QueryWrapper<TAddress> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("user_id", address.getUserId());
        // 调用查询方法
        return addressMapper.selectPage(pageParam, queryWrapper);
    }

    /**
     * 根据用户ID查询该用户的默认地址
     * @param webUserInfoId
     * @return
     */
    @Override
    public TAddress getUserDefaultAddress(Integer webUserInfoId) {
        // 封装查询条件
        QueryWrapper<TAddress> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("user_id", webUserInfoId);
        // 只查默认地址
        queryWrapper.eq("is_default", "1");
        // 调用查询方法
        return addressMapper.selectOne(queryWrapper);
    }
}
