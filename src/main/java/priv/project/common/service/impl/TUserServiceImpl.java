package priv.project.common.service.impl;

import cn.hutool.core.util.StrUtil;
import cn.hutool.crypto.SecureUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.Date;
import java.util.List;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import priv.project.common.error.MyErrorModel;
import priv.project.common.mapper.TUserMapper;
import priv.project.common.entity.TUser;
import priv.project.common.service.TUserService;

/**
 * 用户表Service
 * @author 斗佛
 * @date 2022/11/27
 * @description 下一位读我代码的人, 有任何疑问请联系我, qq: 943701114
 */
@Service
@AllArgsConstructor
public class TUserServiceImpl extends ServiceImpl<TUserMapper, TUser> implements TUserService{

    private TUserMapper userMapper;

    /**
     * 分页查询所有用户信息
     * @param page
     * @param limit
     * @param user
     * @return
     */
    @Override
    public IPage<TUser> getListByPage(Long page, Long limit, TUser user) {
        // 创建分页对象
        IPage<TUser> pageParam = new Page<>(page, limit);
        // 创建查询构造器
        QueryWrapper<TUser> queryWrapper = new QueryWrapper<>();
        queryWrapper.like(!StrUtil.isBlankOrUndefined(user.getNickName()), "nick_name", user.getNickName());
        queryWrapper.like(!StrUtil.isBlankOrUndefined(user.getPhone()), "phone", user.getPhone());
        queryWrapper.eq(!StrUtil.isBlankOrUndefined(user.getUserStatus()), "user_status", user.getUserStatus());
        queryWrapper.orderByDesc("create_date");
        // 调用分页查询方法
        return userMapper.selectPage(pageParam, queryWrapper);
    }

    /**
     * 根据用户名查询该用户的信息
     * @param userName
     * @return
     */
    @Override
    public List<TUser> selectInfoByUserName(String userName) {
        QueryWrapper<TUser> queryWrapper = new QueryWrapper<TUser>();
        queryWrapper.eq(!StrUtil.isBlankOrUndefined(userName), "user_name", userName);
        return userMapper.selectList(queryWrapper);
    }

    /**
     * 保存用户信息
     * @param user
     * @return
     */
    @Override
    public TUser saveInfo(TUser user) {
        // 首先查询该用户名是否已注册
        QueryWrapper<TUser> queryWrapper = new QueryWrapper<>();
        // 查询条件为用户名
        queryWrapper.eq("user_name", user.getUserName());
        // 调用查询方法
        List<TUser> userList = userMapper.selectList(queryWrapper);
        // 判断查询结果是否存在,
        if(userList != null && userList.size() > 0) {
            // 结果大于0 代表已存在
            throw new MyErrorModel("该用户名已注册！");
        }
        // 保存注册时间为当前时间
        user.setCreateDate(new Date());
        // 不存在, 密码进行加密
        user.setPassword(SecureUtil.md5(user.getPassword()));
        // 默认用户状态为正常
        user.setUserStatus("0");
        // 调用保存方法
        this.save(user);
        return user;
    }
}
