package priv.project.common.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import javax.management.Query;
import java.util.List;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import priv.project.common.entity.TUploadFile;
import priv.project.common.mapper.TUploadFileMapper;
import priv.project.common.service.TUploadFileService;
/**
 * @author 斗佛
 * @date 2022/12/5
 **/
@Service
@AllArgsConstructor
public class TUploadFileServiceImpl extends ServiceImpl<TUploadFileMapper, TUploadFile> implements TUploadFileService{

    private TUploadFileMapper uploadFileMapper;

    /**
     * 根据商品ID查询商品下的图片
     * @param goodsId
     * @return
     */
    @Override
    public List<TUploadFile> getUploadListByGoodsId(Integer goodsId) {
        // 创建查询条件
        QueryWrapper<TUploadFile> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("goods_id", goodsId);
        return uploadFileMapper.selectList(queryWrapper);
    }
}
