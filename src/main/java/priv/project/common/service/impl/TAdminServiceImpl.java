package priv.project.common.service.impl;

import cn.hutool.core.util.StrUtil;
import cn.hutool.crypto.SecureUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import priv.project.common.entity.TUser;
import priv.project.common.error.MyErrorModel;
import priv.project.common.mapper.TAdminMapper;
import priv.project.common.entity.TAdmin;
import priv.project.common.mapper.TUserMapper;
import priv.project.common.service.TAdminService;

/**
 * 管理员表Service
 * @author 斗佛
 * @date 2022/11/27
 * @description 下一位读我代码的人, 有任何疑问请联系我, qq: 943701114
 */
@Service
public class TAdminServiceImpl extends ServiceImpl<TAdminMapper, TAdmin> implements TAdminService {

    @Autowired
    private TAdminMapper adminMapper;

    /**
     * 根据用户名查询该用户的信息
     * @param userName
     * @return
     */
    @Override
    public List<TAdmin> selectInfoByUserName(String userName) {
        QueryWrapper<TAdmin> queryWrapper = new QueryWrapper<TAdmin>();
        queryWrapper.eq(!StrUtil.isBlankOrUndefined(userName), "user_name", userName);
        return adminMapper.selectList(queryWrapper);
    }

    /**
     * 分页查询所有信息
     *
     * @param admin
     * @param page
     * @param limit
     * @param adminId
     * @return
     */
    @Override
    public IPage<TAdmin> getListByPage(TAdmin admin, Long page, Long limit, Integer adminId) {
        // 开启分页查询
        IPage<TAdmin> pageBean = new Page<>(page, limit);
        // 创建MyBatisPlus条件对象
        QueryWrapper<TAdmin> queryWrapper = new QueryWrapper<TAdmin>();
        queryWrapper.ne("id", adminId);
        // 调用查询方法
        return adminMapper.selectPage(pageBean, queryWrapper);
    }

    /**
     * 修改信息
     * @param admin
     * @return
     */
    @Override
    public void updateInfo(TAdmin admin) {
        // 首先查询元数据
        TAdmin userInfo = adminMapper.selectById(admin.getId());
        // 判断是否有修改用户名，如果有修改还要查看用户名是否重复
        if (!admin.getUserName().equals(userInfo.getUserName())) {
            // 查询用户名是否已存在
            TAdmin findUserName = adminMapper.selectOne(new QueryWrapper<TAdmin>().eq("user_name", admin.getUserName()));
            if (findUserName != null) {
                throw new RuntimeException("用户名已存在！");
            }
        }
        // 判断原密码和本次要更新的密码是否一致，如果一致代表没有修改密码则不对密码进行处理
        if (!userInfo.getPassword().equals(admin.getPassword())) {
            // 不一致代表有修改密码，则重新加密
            String pwd = SecureUtil.md5(admin.getPassword());
            admin.setPassword(pwd);
        }
        this.updateById(admin);
    }

    /**
     * 删除管理员信息
     * @param ids
     */
    @Override
    public void deleteInfo(List<Integer> ids) {
        // 删除管理员信息
        this.removeByIds(ids);
    }

    /**
     * 新增信息
     * @param admin
     * @return
     */
    @Override
    public void insertInfo(TAdmin admin) {
        // 首先查询用户名是否已设置，如果已设置则不可再设置
        TAdmin userInfo = adminMapper.selectOne(new QueryWrapper<TAdmin>().eq("user_name", admin.getUserName()));
        if (userInfo != null) {
            throw new RuntimeException("用户名已存在！");
        }
        // 不存在则加密密码
        String pwd = SecureUtil.md5(admin.getPassword());
        // 保存加密后的密码
        admin.setPassword(pwd);
        // 调用插入方法
        this.save(admin);
    }
}
