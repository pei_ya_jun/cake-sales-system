package priv.project.common.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import priv.project.common.entity.TGoods;
import priv.project.common.entity.TGoodsType;
import priv.project.common.entity.TUser;
import priv.project.common.error.MyErrorModel;
import priv.project.common.mapper.TGoodsMapper;
import priv.project.common.mapper.TGoodsTypeMapper;
import priv.project.common.service.TGoodsTypeService;

/**
 * 商品类型表Service
 * @author 斗佛
 * @date 2022/11/27
 * @description 下一位读我代码的人, 有任何疑问请联系我, qq: 943701114
 */
@Service
@AllArgsConstructor
public class TGoodsTypeServiceImpl extends ServiceImpl<TGoodsTypeMapper, TGoodsType> implements TGoodsTypeService{

    private TGoodsMapper goodsMapper;

    private TGoodsTypeMapper goodsTypeMapper;

    /**
     * 批量删除类型信息
     * @param split
     */
    @Override
    public void deleteInfo(List<String> split) {
        // 首先查询类型下是否有绑定的商品数据, 如果有不可删除
        QueryWrapper<TGoods> goodsQueryWrapper = new QueryWrapper<>();
        goodsQueryWrapper.in("goods_type_id", split);
        // 调用查询方法
        List<TGoods> tGoods = goodsMapper.selectList(goodsQueryWrapper);
        // 判断查询结果不为空, 代表该类型下已经绑定了商品
        if(tGoods != null && tGoods.size() > 0) {
            throw new MyErrorModel("删除的类型下还有商品, 不可删除! ");
        }
        // 封装要删除的数据信息
        List<TGoodsType> dataList = new ArrayList<>(16);
        // 封装条件到实体类中
        for (int i = 0; i < split.size(); i++) {
            TGoodsType data = new TGoodsType();
            data.setId(Integer.parseInt(split.get(i)));
            // 设置状态改为删除
            data.setIsDelete("1");
            dataList.add(data);
        }
        // 调用更新方法更新删除状态
        this.updateBatchById(dataList);
    }

    /**
     * 分页查询商品类型信息
     *
     * @param page
     * @param limit
     * @param goodsType
     * @return
     */
    @Override
    public IPage<TGoodsType> getListByPage(Long page, Long limit, TGoodsType goodsType) {
        // 创建分页对象
        IPage<TGoodsType> pageParam = new Page<>(page, limit);
        // 创建查询构造器
        QueryWrapper<TGoodsType> queryWrapper = new QueryWrapper<>();
        queryWrapper.like(!StrUtil.isBlankOrUndefined(goodsType.getTypeName()), "type_name", goodsType.getTypeName());
        queryWrapper.eq("is_delete", "0");
        queryWrapper.orderByDesc("create_date");
        // 调用分页查询方法
        return goodsTypeMapper.selectPage(pageParam, queryWrapper);
    }

    /**
     * 查询所有未删除的信息
     * @return
     */
    @Override
    public List<TGoodsType> getListInfo() {
        // 创建查询条件
        QueryWrapper<TGoodsType> queryWrapper = new QueryWrapper<>();
        // 只查询未删除的
        queryWrapper.eq("IS_DELETE", "0");
        return goodsTypeMapper.selectList(queryWrapper);
    }
}
