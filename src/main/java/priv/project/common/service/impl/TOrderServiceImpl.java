package priv.project.common.service.impl;

import cn.hutool.core.date.DateUtil;
import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.*;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import priv.project.common.entity.*;
import priv.project.common.mapper.*;
import priv.project.common.service.TOrderService;

/**
 * 订单表Service
 * @author 斗佛
 * @date 2022/11/27
 * @description 下一位读我代码的人, 有任何疑问请联系我, qq: 943701114
 */
@Service
@AllArgsConstructor
public class TOrderServiceImpl extends ServiceImpl<TOrderMapper, TOrder> implements TOrderService {

    private TOrderMapper orderMapper;

    private TGoodsMapper goodsMapper;

    private TOrderDetailMapper orderDetailMapper;

    private TCartMapper cartMapper;

    private TAddressMapper addressMapper;

    /**
     * 分页查询数据
     * @param page
     * @param limit
     * @param order
     * @return
     */
    @Override
    public IPage<TOrder> getListByPage(Long page, Long limit, TOrder order) {
        // 封装分页参数
        Page<TOrder> pageParam = new Page<>(page, limit);
        // 调用分页查询方法
        IPage<TOrder> listByPage = orderMapper.getListByPage(pageParam, order);
        // 将收货地址进行转换
        if(listByPage != null && listByPage.getRecords() != null) {
            for (TOrder record : listByPage.getRecords()) {
                // 将收货地址转换
                record.setAddressInfo(JSON.parseObject(record.getAddressJsonStr(), TAddress.class));
            }
        }
        return listByPage;
    }

    /**
     * 根据ID查询单条详情
     * @param id
     * @return
     */
    @Override
    public TOrder getInfoById(Integer id) {
        // 根据ID查询单条数据
        TOrder infoById = orderMapper.getInfoById(id);
        // 将收货地址转换
        infoById.setAddressInfo(JSON.parseObject(infoById.getAddressJsonStr(), TAddress.class));
        return infoById;
    }

    /**
     * 根据参数查询销售数据
     * @param startTime
     * @param endTime
     * @return
     */
    @Override
    public Map<String, Object> getDataInfoByParam(Date startTime, Date endTime) {
        // 判断是否传入时间
        if(startTime == null || endTime == null) {
            // 如果没有传入时间, 则默认查询最近七天的数据
            // 首先获取到今天的时间, 当做结束时间
            endTime = new Date();
            // 计算出今天减去6天的时间算出开始时间
            startTime =  DateUtil.offsetDay(new Date(), -6);
        }
        // 最终返回的数据
        Map<String, Object> dataMap = new HashMap<>(16);
        // 调用方法查询数据
        List<Map<String, Object>> dataInfo = orderMapper.getDataInfoByParam(startTime, endTime);
        // 计算出开始时间与结束时间所差天数
        long dayCount = DateUtil.betweenDay(startTime, endTime, true);
        // 循环将所有日期计算出
        List<String> dateList = new ArrayList<>(16);
        // 声明一个新的集合用来重新排序保存数据
        List<BigDecimal> resultList = new ArrayList<>(16);
        for (long i = 0; i <= dayCount; i++) {
            String dateStr = DateUtil.formatDate(DateUtil.offsetDay(startTime, Integer.parseInt(String.valueOf(i))));
            BigDecimal price = null;
            // 循环查询结果
            for (Map<String, Object> item : dataInfo) {
                String createDate = String.valueOf(item.get("createDate"));
                // 如果查询结果中有这个日期下的数据, 则将这条数据直接保存到最终的集合中
                if(createDate != null && dateStr.equals(createDate)) {
                    // 取出当前数据的价格, 除以100计算出价格为元的单位
                    BigDecimal itemPrice = new BigDecimal(item.get("orderPrice").toString());
                    price = itemPrice.divide(new BigDecimal("100"), 3, BigDecimal.ROUND_HALF_UP);
                    break;
                }
            }
            // 如果循环完发现没有这条数据, 则创建一条该日期下的数据, 销售额设置为0
            if(price == null) {
                price = new BigDecimal("0.00");
            }
            // 将数据保存到最终的集合中
            resultList.add(price);
            // 将计算出的日期保存
            dateList.add(dateStr);
        }
        dataMap.put("dateList", dateList);
        dataMap.put("dataInfo", resultList);
        return dataMap;
    }

    /**
     * 单个商品下单
     * @param addressId
     * @param webUserInfo
     */
    @Override
    public void saveInfoByOne(Integer addressId, Integer goodsId, TUser webUserInfo) {
        // 查询商品信息
        TGoods goodsInfo = goodsMapper.getInfoById(goodsId);
        // 查询收货地址信息
        TAddress address = addressMapper.selectById(addressId);
        // 生成订单ID
        Long orderNum = System.currentTimeMillis();
        // 生成订单
        TOrder order = new TOrder();
        order.setOrderNum(orderNum + "");
        order.setUserId(webUserInfo.getId());
        order.setAddressId(addressId);
        order.setUserNickName(webUserInfo.getNickName());
        order.setAddressName(address.getAddressName());
        order.setCreateDate(new Date());
        order.setAddressJsonStr(JSON.toJSONString(address));
        order.setDeliveryStatus("0");
        order.setUserDelFlag("0");
        // 计算订单价格
        order.setOrderPrice(goodsInfo.getGoodsDiscountPrice() == null ? goodsInfo.getGoodsPrice() : goodsInfo.getGoodsDiscountPrice());
        // 插入订单
        orderMapper.insert(order);

        // 创建订单详情信息
        TOrderDetail orderDetail = new TOrderDetail();
        orderDetail.setGoodsId(goodsId);
        orderDetail.setGoodsName(goodsInfo.getGoodsName());
        orderDetail.setGoodsNum(1);
        orderDetail.setGoodsPrice(goodsInfo.getGoodsDiscountPrice() == null ? goodsInfo.getGoodsPrice() : goodsInfo.getGoodsDiscountPrice());
        orderDetail.setTotalPrice(orderDetail.getGoodsPrice() * orderDetail.getGoodsNum());
        orderDetail.setOrderId(order.getId());
        // 插入订单详情
        orderDetailMapper.insert(orderDetail);
    }

    /**
     * 购物车下单
     *
     * @param addressId
     * @param webUserInfo
     */
    @Override
    public void saveInfoByCart(Integer addressId, TUser webUserInfo) {
        // 查询购物车信息
        List<TCart> cartInfo = cartMapper.getCartInfoByUserId(webUserInfo.getId());
        // 查询收货地址信息
        TAddress address = addressMapper.selectById(addressId);
        // 生成订单ID
        Long orderNum = System.currentTimeMillis();
        // 生成订单
        TOrder order = new TOrder();
        order.setOrderNum(orderNum + "");
        order.setUserId(webUserInfo.getId());
        order.setAddressId(addressId);
        order.setUserNickName(webUserInfo.getNickName());
        order.setAddressName(address.getAddressName());
        order.setCreateDate(new Date());
        order.setAddressJsonStr(JSON.toJSONString(address));
        order.setDeliveryStatus("0");
        order.setUserDelFlag("0");
        // 先保存订单信息
        orderMapper.insert(order);
        // 计算总价
        Integer orderPrice = 0;
        for (TCart item : cartInfo) {
            Integer goodsDiscountPrice = item.getGoods().getGoodsDiscountPrice();
            Integer goodsPrice = item.getGoods().getGoodsPrice();
            Integer price = goodsDiscountPrice != null ? goodsDiscountPrice * item.getGoodsCount() : goodsPrice * item.getGoodsCount();
            // 累加金额
            orderPrice += price;
            // 创建订单详情信息
            TOrderDetail orderDetail = new TOrderDetail();
            orderDetail.setTotalPrice(price);
            orderDetail.setOrderId(order.getId());
            orderDetail.setGoodsId(item.getGoodsId());
            orderDetail.setGoodsNum(item.getGoodsCount());
            orderDetail.setGoodsName(item.getGoods().getGoodsName());
            orderDetail.setGoodsPrice(goodsDiscountPrice == null ? goodsPrice : goodsDiscountPrice);
            // 保存订单详情信息
            orderDetailMapper.insert(orderDetail);
        }
        // 更新订单总价
        order.setOrderPrice(orderPrice);
        orderMapper.updateById(order);

        // 清空购物车
        QueryWrapper<TCart> delCart = new QueryWrapper<>();
        delCart.eq("user_id", webUserInfo.getId());
        cartMapper.delete(delCart);
    }
}
