package priv.project.common.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import priv.project.common.entity.TMenu;
import priv.project.common.mapper.TMenuMapper;
import priv.project.common.service.TMenuService;

/**
 * 菜单表Service
 * @author 斗佛
 * @date 2022/11/27
 * @description 下一位读我代码的人, 有任何疑问请联系我, qq: 943701114
 */
@Service
@AllArgsConstructor
public class TMenuServiceImpl extends ServiceImpl<TMenuMapper, TMenu> implements TMenuService{

    private final TMenuMapper menuMapper;

    /**
     * 根据查询参数查询菜单信息
     * @param queryWrapper
     * @return
     */
    @Override
    public List<TMenu> getListByParam(QueryWrapper<TMenu> queryWrapper) {
        return menuMapper.selectList(queryWrapper);
    }
}
