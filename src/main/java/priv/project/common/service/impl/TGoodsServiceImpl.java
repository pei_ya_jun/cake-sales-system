package priv.project.common.service.impl;

import cn.hutool.core.io.FileUtil;
import cn.hutool.core.lang.UUID;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.util.ResourceUtils;
import org.springframework.web.multipart.MultipartFile;
import priv.project.common.entity.TUploadFile;
import priv.project.common.error.MyErrorModel;
import priv.project.common.mapper.TGoodsMapper;
import priv.project.common.entity.TGoods;
import priv.project.common.mapper.TUploadFileMapper;
import priv.project.common.service.TGoodsService;
import priv.project.common.util.PublicUtil;
import priv.project.common.util.ServletUtils;

/**
 * 商品表Service
 * @author 斗佛
 * @date 2022/11/27
 * @description 下一位读我代码的人, 有任何疑问请联系我, qq: 943701114
 */
@Service
@AllArgsConstructor
public class TGoodsServiceImpl extends ServiceImpl<TGoodsMapper, TGoods> implements TGoodsService{

    private TGoodsMapper goodsMapper;

    private TUploadFileMapper uploadFileMapper;

    /**
     * 分页查询信息
     * @param page
     * @param limit
     * @param goods
     * @return
     */
    @Override
    public IPage<TGoods> getListByPage(Long page, Long limit, TGoods goods) {
        // 封装分页信息
        IPage<TGoods> pageParam = new Page<>(page, limit);
        // 调用mapper层方法
        return goodsMapper.getListByPage(pageParam, goods);
    }

    /**
     * 根据参数查询所有已上架商品
     * @param goods
     * @return
     */
    @Override
    public List<TGoods> getListByParam(TGoods goods) {
        // 调用mapper层方法
        return goodsMapper.getListByParam(goods);
    }

    /**
     * 查询首页推荐的商品
     * @return
     */
    @Override
    public List<TGoods> getIndexGoodsList() {
        return goodsMapper.getIndexGoodsList();
    }

    /**
     * 查询最新发布的十个商品
     * @return
     */
    @Override
    public List<TGoods> getNewsGoodsList() {
        return goodsMapper.getNewsGoodsList();
    }

    /**
     * 根据ID查询单条信息
     * @param id
     * @return
     */
    @Override
    public TGoods getInfoById(Integer id) {
        // 调用mapper层方法
        return goodsMapper.getInfoById(id);
    }

    /**
     * 删除商品
     * @param split
     */
    @Override
    public void deleteInfo(List<String> split) {
        // 循环将所有要删除的ID绑定
        for (String id : split) {
            TGoods goods = new TGoods();
            goods.setId(Integer.valueOf(id));
            // 设置状态为已删除
            goods.setIsDelete("1");
            // 调用更新方法, 更新删除状态
            goodsMapper.updateById(goods);
        }
    }

    /**
     * 保存商品信息
     * @param files
     * @param goods
     */
    @Override
    public void insertInfo(MultipartFile[] files, TGoods goods, HttpServletRequest request) throws IOException {
        // 首先调用新增方法将商品信息保存进数据库
        goods.setIsDelete("0");
        goods.setIndexFlag("0");
        goods.setGoodsStatus("1");
        goodsMapper.insert(goods);
        // 循环上传文件
        for (MultipartFile file : files) {
            // 获取文件存放路径
            String path = ResourceUtils.getURL("classpath:").getPath().replace("%20"," ")+"/static/upload/";

            // 判断文件是否为空
            if (file.isEmpty() || file.getSize() <= 0) {
                throw new MyErrorModel("上传文件为空！");
            }
            // 调用上传文件方法
            String fileName = PublicUtil.uploadImageFile(path, file);
            // 拼接文件上传后的访问地址
            String fileUrl = ServletUtils.getProjectHttpUrl(request, false) + "upload/" + fileName;
            // 创建文件上传对象, 保存上传后的文件数据
            TUploadFile uploadFile = new TUploadFile();
            uploadFile.setGoodsId(goods.getId());
            uploadFile.setFileName(fileName);
            uploadFile.setFileSize(file.getSize());
            uploadFile.setFileUrl(fileUrl);
            // 调用新增方法
            uploadFileMapper.insert(uploadFile);
        }
    }

    /**
     * 修改信息
     *
     * @param files
     * @param goods
     * @param request
     */
    @Override
    public void updateInfo(MultipartFile[] files, TGoods goods, HttpServletRequest request) throws IOException {
        // 首先调用方法将商品信息更新
        goodsMapper.updateById(goods);
        // 获取文件存放路径
        String path = ResourceUtils.getURL("classpath:").getPath().replace("%20"," ")+"/static/upload/";
        // 如果有需要删除的文件则循环删除
        if(goods.getWaitDeleteList() != null && goods.getWaitDeleteList().size() > 0) {
            for (TUploadFile delFile : goods.getWaitDeleteList()) {
                // 循环调用删除
                FileUtil.del(new File(path + delFile.getFileName()));
                // 调用数据库删除
                uploadFileMapper.deleteById(delFile.getId());
            }
        }
        // 循环上传文件
        if(files != null && files.length > 0) {
            for (MultipartFile file : files) {
                // 判断文件是否为空
                if (file.isEmpty() || file.getSize() <= 0) {
                    throw new MyErrorModel("上传文件为空！");
                }
                // 调用上传文件方法
                String fileName = PublicUtil.uploadImageFile(path, file);
                // 拼接文件上传后的访问地址
                String fileUrl = ServletUtils.getProjectHttpUrl(request, false) + "upload/" + fileName;
                // 创建文件上传对象, 保存上传后的文件数据
                TUploadFile uploadFile = new TUploadFile();
                uploadFile.setGoodsId(goods.getId());
                uploadFile.setFileName(fileName);
                uploadFile.setFileSize(file.getSize());
                uploadFile.setFileUrl(fileUrl);
                // 调用新增方法
                uploadFileMapper.insert(uploadFile);
            }
        }
    }
}
