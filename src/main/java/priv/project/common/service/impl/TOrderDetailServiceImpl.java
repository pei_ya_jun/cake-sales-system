package priv.project.common.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import priv.project.common.entity.TOrderDetail;
import priv.project.common.mapper.TOrderDetailMapper;
import priv.project.common.service.TOrderDetailService;

/**
 * 订单详情表Service
 * @author 斗佛
 * @date 2022/11/27
 * @description 下一位读我代码的人, 有任何疑问请联系我, qq: 943701114
 */
@Service
@AllArgsConstructor
public class TOrderDetailServiceImpl extends ServiceImpl<TOrderDetailMapper, TOrderDetail> implements TOrderDetailService{

    private final TOrderDetailMapper orderDetailMapper;

    /**
     * 分页查询信息
     * @param page
     * @param limit
     * @param orderDetail
     * @return
     */
    @Override
    public IPage<TOrderDetail> getListByPage(Long page, Long limit, TOrderDetail orderDetail) {
        // 封装分页条件
        Page<TOrderDetail> pageParam = new Page<>(page, limit);
        // 创建查询对象
        QueryWrapper<TOrderDetail> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq(orderDetail.getOrderId() != null, "order_id", orderDetail.getOrderId());
        // 调用查询方法
        return orderDetailMapper.selectPage(pageParam, queryWrapper);
    }
}
