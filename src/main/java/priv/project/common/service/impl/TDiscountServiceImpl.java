package priv.project.common.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import priv.project.common.entity.TDiscount;
import priv.project.common.error.MyErrorModel;
import priv.project.common.mapper.TDiscountMapper;
import priv.project.common.service.TDiscountService;

/**
 * 折扣表Service
 * @author 斗佛
 * @date 2022/11/27
 * @description 下一位读我代码的人, 有任何疑问请联系我, qq: 943701114
 */
@Service
@AllArgsConstructor
public class TDiscountServiceImpl extends ServiceImpl<TDiscountMapper, TDiscount> implements TDiscountService{

    private TDiscountMapper discountMapper;

    /**
     * 分页查询信息
     * @param page
     * @param limit
     * @param discount
     * @return
     */
    @Override
    public IPage<TDiscount> getListByPage(Long page, Long limit, TDiscount discount) {
        // 封装分页参数
        IPage<TDiscount> pageParam = new Page<>();
        return discountMapper.getListByPage(pageParam, discount);
    }

    /**
     * 删除信息
     * @param split
     */
    @Override
    public void deleteInfo(List<Integer> split) {
        // 首先判断要删除的活动是否包含已经活动开始的
        List<TDiscount> listByParam = discountMapper.getListByParam(split);
        // 判断是否有查询结果
        if(listByParam != null && listByParam.size() > 0) {
            throw new MyErrorModel("包含已开始的活动！不可删除！");
        }
        // 没有已开始的活动, 调用删除
        this.removeByIds(split);
    }

    /**
     * 新增活动信息
     * @param discount
     */
    @Override
    public void saveInfo(TDiscount discount) {
        // 首先判断该商品在这个时间段是不是已经有了活动
        // 如果已经有别的活动则不可以创建
        List<TDiscount> discountSelect = discountMapper.getInfoByDateAndGoodsId(discount);
        if(discountSelect != null && discountSelect.size() > 0) {
            throw new MyErrorModel("该商品在这个时间内已经有其他活动!");
        }
        // 没问题则保存信息
        discountMapper.insert(discount);
    }

    /**
     * 修改活动信息
     *
     * @param discount
     */
    @Override
    public void updateInfo(TDiscount discount) {
        // 首先判断该商品在这个时间段是不是已经有了活动
        // 如果已经有别的活动则不可以创建
        List<TDiscount> discountSelect = discountMapper.getInfoByDateAndGoodsId(discount);
        if(discountSelect != null && discountSelect.size() > 0) {
            throw new MyErrorModel("该商品在这个时间内已经有其他活动!");
        }
        // 没问题则保存信息
        discountMapper.updateById(discount);
    }

    /**
     * 根据ID查询详情
     * @param id
     * @return
     */
    @Override
    public TDiscount getInfoById(Integer id) {
        return discountMapper.getInfoById(id);
    }
}
