package priv.project.common.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import priv.project.common.entity.TGoods;
import priv.project.common.mapper.TCartMapper;
import priv.project.common.entity.TCart;
import priv.project.common.mapper.TGoodsMapper;
import priv.project.common.service.TCartService;

/**
 * 购物车表Service
 * @author 斗佛
 * @date 2022/11/27
 * @description 下一位读我代码的人, 有任何疑问请联系我, qq: 943701114
 */
@Service
@AllArgsConstructor
public class TCartServiceImpl extends ServiceImpl<TCartMapper, TCart> implements TCartService{

    private final TCartMapper cartMapper;

    private final TGoodsMapper goodsMapper;

    /**
     * 根据用户ID查询用户购物车信息
     * @param webUserInfoId
     * @return
     */
    @Override
    public List<TCart> getCartInfoByUserId(Integer webUserInfoId) {
        // 调用Mapper层方法
        return cartMapper.getCartInfoByUserId(webUserInfoId);
    }

    /**
     * 加入购物车
     * @param goodsId
     * @param webUserInfoId
     */
    @Override
    public void saveCartInfo(Integer goodsId, Integer webUserInfoId) {
        // 查询商品信息
        TGoods goodsInfo = goodsMapper.getInfoById(goodsId);
        // 首先根据用户ID和商品ID查询该商品是否已经加入购物车
        QueryWrapper<TCart> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("user_id", webUserInfoId);
        queryWrapper.eq("goods_id", goodsId);
        // 调用查询
        TCart tCart = cartMapper.selectOne(queryWrapper);
        if(tCart == null) {
            // 不存在
            // 创建一条新的购物车信息
            tCart = new TCart();
            tCart.setUserId(webUserInfoId);
            tCart.setGoodsId(goodsId);
            tCart.setGoodsCount(1);
            tCart.setGoodsTotal(goodsInfo.getGoodsDiscountPrice() != null ? goodsInfo.getGoodsDiscountPrice() : goodsInfo.getGoodsPrice());
        } else {
            // 存在, 直接在之前的数量上+1
            tCart.setGoodsCount(tCart.getGoodsCount() + 1);
            // 重新计算小计
            tCart.setGoodsTotal(goodsInfo.getGoodsDiscountPrice() != null ? goodsInfo.getGoodsDiscountPrice() * tCart.getGoodsCount() : goodsInfo.getGoodsPrice() * tCart.getGoodsCount());
        }
        this.saveOrUpdate(tCart);
    }
}
