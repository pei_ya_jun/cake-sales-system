package priv.project.common.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import priv.project.common.entity.TMenu;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * 菜单表Service接口层
 * @author 斗佛
 * @date 2022/11/27
 * @description 下一位读我代码的人, 有任何疑问请联系我, qq: 943701114
 */
public interface TMenuService extends IService<TMenu>{

    /**
     * 根据查询参数查询菜单信息
     * @param queryWrapper
     * @return
     */
    List<TMenu> getListByParam(QueryWrapper<TMenu> queryWrapper);
}
