package priv.project.common.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import priv.project.common.entity.TAdmin;
import com.baomidou.mybatisplus.extension.service.IService;
import priv.project.common.entity.TUser;

import java.util.List;

/**
 * 管理员表Service接口层
 * @author 斗佛
 * @date 2022/11/27
 * @description 下一位读我代码的人, 有任何疑问请联系我, qq: 943701114
 */
public interface TAdminService extends IService<TAdmin>{

    /**
     * 根据用户名查询该用户的信息
     * @param userName
     * @return
     */
    List<TAdmin> selectInfoByUserName(String userName);

    /**
     * 分页查询所有信息
     * @param admin
     * @param page
     * @param limit
     * @param adminId
     * @return
     */
    IPage<TAdmin> getListByPage(TAdmin admin, Long page, Long limit, Integer adminId);

    /**
     * 新增管理员信息
     * @param admin
     */
    void insertInfo(TAdmin admin);

    /**
     * 修改管理员信息
     * @param admin
     */
    void updateInfo(TAdmin admin);

    /**
     * 删除管理员信息
     * @param ids
     */
    void deleteInfo(List<Integer> ids);
}
