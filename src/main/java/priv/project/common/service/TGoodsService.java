package priv.project.common.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import org.springframework.web.multipart.MultipartFile;
import priv.project.common.entity.TGoods;
import com.baomidou.mybatisplus.extension.service.IService;

import javax.servlet.http.HttpServletRequest;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

/**
 * 商品表Service接口层
 * @author 斗佛
 * @date 2022/11/27
 * @description 下一位读我代码的人, 有任何疑问请联系我, qq: 943701114
 */
public interface TGoodsService extends IService<TGoods>{

    /**
     * 分页查询信息
     * @param page
     * @param limit
     * @param goods
     * @return
     */
    IPage<TGoods> getListByPage(Long page, Long limit, TGoods goods);

    /**
     * 根据ID查询单条信息
     * @param id
     * @return
     */
    TGoods getInfoById(Integer id);

    /**
     * 删除商品
     * @param split
     */
    void deleteInfo(List<String> split);

    /**
     * 保存商品信息
     * @param files
     * @param goods
     */
    void insertInfo(MultipartFile[] files, TGoods goods, HttpServletRequest request) throws IOException;

    /**
     * 修改信息
     * @param files
     * @param goods
     * @param request
     */
    void updateInfo(MultipartFile[] files, TGoods goods, HttpServletRequest request) throws IOException;

    /**
     * 根据参数查询所有已上架商品
     * @param goods
     * @return
     */
    List<TGoods> getListByParam(TGoods goods);

    /**
     * 查询首页推荐的商品
     * @return
     */
    List<TGoods> getIndexGoodsList();

    /**
     * 查询最新发布的十个商品
     * @return
     */
    List<TGoods> getNewsGoodsList();

}
