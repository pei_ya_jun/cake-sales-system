package priv.project.common.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import priv.project.common.entity.TDiscount;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * 折扣表Service接口层
 * @author 斗佛
 * @date 2022/11/27
 * @description 下一位读我代码的人, 有任何疑问请联系我, qq: 943701114
 */
public interface TDiscountService extends IService<TDiscount>{

    /**
     * 分页查询信息
     * @param page
     * @param limit
     * @param discount
     * @return
     */
    IPage<TDiscount> getListByPage(Long page, Long limit, TDiscount discount);

    /**
     * 删除信息
     * @param split
     */
    void deleteInfo(List<Integer> split);

    /**
     * 新增活动信息
     * @param discount
     */
    void saveInfo(TDiscount discount);

    /**
     * 修改活动信息
     * @param discount
     */
    void updateInfo(TDiscount discount);

    /**
     * 根据ID查询详情
     * @param id
     * @return
     */
    TDiscount getInfoById(Integer id);
}
