package priv.project.common.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import priv.project.common.entity.TGoodsType;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * 商品类型表Service接口层
 * @author 斗佛
 * @date 2022/11/27
 * @description 下一位读我代码的人, 有任何疑问请联系我, qq: 943701114
 */
public interface TGoodsTypeService extends IService<TGoodsType>{

    /**
     * 批量删除类型信息
     * @param split
     */
    void deleteInfo(List<String> split);

    /**
     * 分页查询商品类型信息
     * @param page
     * @param limit
     * @param goodsType
     * @return
     */
    IPage<TGoodsType> getListByPage(Long page, Long limit, TGoodsType goodsType);

    /**
     * 查询所有未删除的信息
     * @return
     */
    List<TGoodsType> getListInfo();
}
