package priv.project.common.service;

import priv.project.common.entity.TCart;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * 购物车表Service接口层
 * @author 斗佛
 * @date 2022/11/27
 * @description 下一位读我代码的人, 有任何疑问请联系我, qq: 943701114
 */
public interface TCartService extends IService<TCart>{

    /**
     * 根据用户ID查询用户购物车信息
     * @param webUserInfoId
     * @return
     */
    List<TCart> getCartInfoByUserId(Integer webUserInfoId);

    /**
     * 加入购物车
     * @param goodsId
     * @param webUserInfoId
     */
    void saveCartInfo(Integer goodsId, Integer webUserInfoId);
}
