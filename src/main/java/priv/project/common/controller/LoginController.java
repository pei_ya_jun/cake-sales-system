package priv.project.common.controller;

import cn.hutool.captcha.CaptchaUtil;
import cn.hutool.captcha.LineCaptcha;
import cn.hutool.core.lang.UUID;
import cn.hutool.core.util.StrUtil;
import cn.hutool.crypto.SecureUtil;
import com.alibaba.fastjson.JSON;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import priv.project.common.entity.ResultMessage;
import priv.project.common.entity.TAdmin;
import priv.project.common.entity.TMenu;
import priv.project.common.entity.TUser;
import priv.project.common.error.MyErrorModel;
import priv.project.common.service.TAdminService;
import priv.project.common.service.TUserService;
import priv.project.common.util.ServletUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;
import java.util.Map;

import static java.util.concurrent.TimeUnit.MINUTES;

/**
 * @author 斗佛
 * @date 2022/11/27
 * @description 下一位读我代码的人, 有任何疑问请联系我, qq: 943701114
 * 登录控制器
 **/
@RestController
@RequestMapping("/rest/login")
public class LoginController {

    @Autowired
    private TAdminService adminService;

    @Autowired
    private TUserService userService;

    /**
     * 登录验证 - 管理员
     * @param userName
     * @param password
     * @return
     */
    @PostMapping("/adminLogin")
    public ResultMessage<TAdmin> loginVerify(String userName, String password, String verifyCode, HttpSession session) {
        // 比对验证码
        String verifyCodeSession = session.getAttribute("verifyCode")+"";
        if(!verifyCode.equalsIgnoreCase(verifyCodeSession)) {
            return new ResultMessage<TAdmin>().warn("验证码输入有误！");
        }
        // 首先根据用户名查询该用户的信息
        List<TAdmin> adminListByUserName = adminService.selectInfoByUserName(userName);
        // 查询结果为空直接返回
        if(adminListByUserName == null || adminListByUserName.size() <= 0) {
            return new ResultMessage<TAdmin>().warn("用户不存在！");
        }
        // 不为空，拼接密码与加密串
        TAdmin resultAdmin = adminListByUserName.get(0);
        // 加密密码后进行比对密码加密规则：MD5
        String newPassword = SecureUtil.md5(password);
        // 对比密码是否正确
        if(!resultAdmin.getPassword().equals(newPassword)) {
            // 不正确，返回错误
            return new ResultMessage<TAdmin>().warn("用户名或密码有误！");
        }
        // 密码正确
        // 清空密码
        resultAdmin.setPassword("");
        // 保存登录信息在session中
        session.setAttribute("userInfo", resultAdmin);
        return new ResultMessage<TAdmin>().success("登陆成功！", resultAdmin);
    }

    /**
     * 登录验证 - 用户
     * @param userName
     * @param password
     * @return
     */
    @PostMapping("/userLogin")
    public ResultMessage<TUser> userLogin(String userName, String password, HttpSession session) {
        // 首先根据用户名查询该用户的信息
        List<TUser> userList = userService.selectInfoByUserName(userName);
        // 查询结果为空直接返回
        if(userList == null || userList.size() <= 0) {
            return new ResultMessage<TUser>().warn("用户不存在！");
        }
        // 不为空，拼接密码与加密串
        TUser user = userList.get(0);
        // 加密密码后进行比对密码加密规则：MD5
        String newPassword = SecureUtil.md5(password);
        // 对比密码是否正确
        if(!user.getPassword().equals(newPassword)) {
            // 不正确，返回错误
            return new ResultMessage<TUser>().warn("用户名或密码有误！");
        }
        // 判断该账号是否已冻结, 如果冻结则不可以登录
        if("1".equals(user.getUserStatus())) {
            return new ResultMessage<TUser>().warn("您的账号已被冻结！无法登陆！");
        }
        // 密码正确
        // 清空密码
        user.setPassword("");
        // 保存登录信息在session中
        session.setAttribute("webUserInfo", user);
        return new ResultMessage<TUser>().success("登陆成功！", user);
    }

    /**
     * 获取登录用户信息
     * @return
     */
    @PostMapping("getLoginUserInfo")
    public ResultMessage<TUser> getLoginUserInfo(HttpServletRequest request) {
        try {
            return new ResultMessage<TUser>().success("登陆成功！", ServletUtils.getWebUserInfo(request.getSession()));
        } catch (Exception e) {
            e.printStackTrace();
            return new ResultMessage<TUser>().danger("后台错误！");
        }
    }

    /**
     * 退出登录 - 用户
     * @return
     */
    @PostMapping("/userLoginOut")
    public ResultMessage<TUser> userLoginOut(HttpSession session) {
        // 将当前登录用户的会话关闭
        session.removeAttribute("webUserInfo");
        return new ResultMessage<TUser>().success("已安全退出！");
    }


    /**
     * 获取图形验证码
     * @param type  代表生成验证码的操作类型, 0代表是用户登录时生成的, 1代表是用户注册生成, 否则是管理员登录生成的验证码
     */
    @GetMapping("/getVerifyImg")
    public void verifyImg(HttpSession session, HttpServletResponse response, Integer type) {
        // 定义图形验证码的长和宽
        LineCaptcha lineCaptcha = CaptchaUtil.createLineCaptcha(86, 44);
        // 保存验证码内容
        System.out.println("验证码为： " + lineCaptcha.getCode());
        if(type == 0) {
            session.setAttribute("userVerifyCode", lineCaptcha.getCode());
        } else if(type == 1) {
            session.setAttribute("userRegister", lineCaptcha.getCode());
        } else {
            session.setAttribute("verifyCode", lineCaptcha.getCode());
        }
        OutputStream outputStream = null;
        // 写出流
        try {
            outputStream = response.getOutputStream();
            response.setContentType("image/png");
            lineCaptcha.write(outputStream);
            outputStream.flush();
            outputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if(outputStream != null) {
                try {
                    outputStream.flush();
                    outputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

}
