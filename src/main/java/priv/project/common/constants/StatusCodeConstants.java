package priv.project.common.constants;

/**
 * @author 斗佛
 * @date 2022/11/27
 * @description 下一位读我代码的人, 有任何疑问请联系我, qq: 943701114
 * 返回状态码统一定义
 */
public interface StatusCodeConstants {
    /**
     * 接口返回的状态码
     */
    public enum STATUS_CODE {
        /**
         * 后台错误
         */
        ERROR(0),
        /**
         * 请求成功
         */
        SUCCESS(1),
        /**
         * Layui表格查询成功返回的状态
         */
        SUCCESS_LAYUI(0),
        /**
         * 请求失败(自定义错误)
         */
        WARN(2),
        /**
         * token校验失败
         */
        TOKEN_ERROR(3);

        private final Integer value;

        STATUS_CODE(Integer value) {
            this.value = value;
        }

        public Integer getValue() {
            return value;
        }
    }

}
